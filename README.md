# Phalcon NG

[![PDS Skeleton](https://img.shields.io/badge/pds-skeleton-blue.svg?style=flat-square)](https://github.com/php-pds/skeleton)

This repository is a testbed for Phalcon.

It is **ONLY** a testbed and nothing more.

Components can be created here and tested before ported to Zephir and being part of Phalcon. This repository also aims to increase code coverage for Phalcon.

<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Logger;

/**
 * PhalconNG\Logger\Item
 *
 * Represents each item in a logging transaction
 *
 */
class Item
{
    /**
     * Interpolation context
     *
     * @var array
     */
    private $context = [];

    /**
     * Log message
     *
     * @var string
     */
    private $message;

    /**
     * Log message
     *
     * @var string
     */
    private $name;

    /**
     * Log timestamp
     *
     * @var integer
     */
    private $time;

    /**
     * Log type
     *
     * @var integer
     */
    private $type;

    /**
     * PhalconNG\Logger\Item constructor
     *
     * @param string $message
     * @param string $name
     * @param int    $type
     * @param int    $time
     * @param array  $context
     */
    public function __construct(
        string $message,
        string $name,
        int $type,
        int $time = 0,
        array $context = []
    ) {
        $this->message = $message;
        $this->name    = $name;
        $this->type    = $type;
        $this->time    = $time;
        $this->context = $context;
    }

    /**
     * @return array
     */
    public function getContext(): array
    {
        return $this->context;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getTime(): int
    {
        return $this->time;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }
}

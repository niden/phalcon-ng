<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Logger\Formatter;

abstract class AbstractFormatter implements FormatterInterface
{
    /**
     * Interpolates context values into the message placeholders
     *
     * @param string $message
     * @param array  $context
     *
     * @return string
     * @see http://www.php-fig.org/psr/psr-3/ Section 1.2 Message
     */
    public function interpolate(string $message, array $context = []): string
    {
        if (count($context) > 0) {
            $replace = [];
            foreach ($context as $key => $value) {
                $replace['{' . $key . '}'] = $value;
            }

            return strtr($message, $replace);
        }
        return $message;
    }
}

<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Logger\Formatter;

use PhalconNG\Logger\Item;

/**
 * PhalconNG\Logger\FormatterInterface
 *
 * This interface must be implemented by formatters in PhalconNG\Logger
 */
interface FormatterInterface
{
    /**
     * Applies a format to an item
     *
     * @param Item $item
     *
     * @return string|array
     */
    public function format(Item $item);
}

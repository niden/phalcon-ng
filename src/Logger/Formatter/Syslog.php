<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Logger\Formatter;

use PhalconNG\Logger\Item;

/**
 * PhalconNG\Logger\Formatter\Syslog
 *
 * Prepares a message to be used in a Syslog backend
 */
class Syslog extends AbstractFormatter
{
    /**
     * Applies a format to a message before sent it to the internal log
     *
     * @param Item $item
     *
     * @return array
     */
    public function format(Item $item): array
    {
        if (count($item->getContext()) > 0) {
            $message = $this->interpolate($item->getMessage(), $item->getContext());
        } else {
            $message = $item->getMessage();
        }

        return [$item->getType(), $message];
    }
}

<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Logger\Formatter;

use PhalconNG\Logger\Formatter\Formatter;
use PhalconNG\Logger\Item;
use function str_replace;
use function strpos;

/**
 * PhalconNG\Logger\Formatter\Line
 *
 * Formats messages using an one-line string
 */
class Line extends AbstractFormatter
{
    /**
     * Default date format
     *
     * @var string
     */
    protected $dateFormat = 'D, d M y H:i:s O';

    /**
     * Format applied to each message
     *
     * @var string
     */
    protected $format = '[%date%][%type%] %message%';

    /**
     * PhalconNG\Logger\Formatter\Line construct
     *
     * @param string $format
     * @param string $dateFormat
     */
    public function __construct(string $format = '', string $dateFormat = '')
    {
        if (true !== empty($format)) {
            $this->format = $format;
        }
        if (true !== empty($dateFormat)) {
            $this->dateFormat = $dateFormat;
        }
    }

    /**
     * Applies a format to a message before sent it to the internal log
     *
     * @param Item $item
     *
     * @return string
     */
    public function format(Item $item): string
    {
        $formatted = $this->format;

        /**
         * Check if the format has the %date% placeholder
         */
        if (false !== strpos($formatted, '%date%')) {
            $formatted = str_replace(
                '%date%',
                date(
                    $this->dateFormat,
                    $item->getTime()
                ),
                $formatted
            );
        }

        /**
         * Check if the format has the %type% placeholder
         */
        if (false !== strpos($formatted, '%type%')) {
            $formatted = str_replace('%type%', $item->getName(), $formatted);
        }

        $formatted = str_replace('%message%', $item->getMessage(), $formatted) . PHP_EOL;

        if (count($item->getContext()) > 0) {
            return $this->interpolate(
                $formatted,
                $item->getContext()
            );
        }

        return $formatted;
    }

    /**
     * @return string
     */
    public function getDateFormat(): string
    {
        return $this->dateFormat;
    }

    /**
     * @param string $format
     */
    public function setDateFormat(string $format): void
    {
        $this->dateFormat = $format;
    }

    /**
     * @return string
     */
    public function getFormat(): string
    {
        return $this->format;
    }

    /**
     * @param string $format
     */
    public function setFormat(string $format): void
    {
        $this->format = $format;
    }
}

<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Logger\Formatter;

use PhalconNG\Logger\Item;

/**
 * PhalconNG\Logger\Formatter\Json
 *
 * Formats messages using JSON encoding
 */
class Json extends AbstractFormatter
{
    /**
     * Default date format
     *
     * @var string
     */
    protected $dateFormat = 'D, d M y H:i:s O';

    /**
     * PhalconNG\Logger\Formatter\Json construct
     *
     * @param string $dateFormat
     */
    public function __construct(string $dateFormat = '')
    {
        if (true !== empty($dateFormat)) {
            $this->dateFormat = $dateFormat;
        }
    }

    /**
     * Applies a format to a message before sent it to the internal log
     *
     * @param Item $item
     *
     * @return string
     */
    public function format(Item $item): string
    {
        if (count($item->getContext()) > 0) {
            $message = $this->interpolate(
                $item->getMessage(),
                $item->getContext()
            );
        } else {
            $message = $item->getMessage();
        }

        return json_encode(
            [
                    'type'      => $item->getName(),
                    'message'   => $message,
                    'timestamp' => date($this->dateFormat, $item->getTime()),
                ]
        ) . PHP_EOL;
    }

    /**
     * @return string
     */
    public function getDateFormat(): string
    {
        return $this->dateFormat;
    }

    /**
     * @param string $format
     */
    public function setDateFormat(string $format): void
    {
        $this->dateFormat = $format;
    }
}

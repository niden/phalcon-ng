<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Logger\Adapter;

use PhalconNG\Logger\Item;

/**
 * PhalconNG\Logger\Adapter\Noop
 *
 * Adapter to store logs in plain text files
 */
class Noop extends AbstractAdapter
{
    /**
     * Closes the stream
     */
    public function close(): bool
    {
        return true;
    }

    /**
     * Processes the message i.e. writes it to the file
     *
     * @param Item $item
     */
    public function process(Item $item): void
    {
        // noop
    }
}

<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Logger\Adapter;

use PhalconNG\Helper\Str;
use PhalconNG\Logger\Exception;
use PhalconNG\Logger\Item;
use UnexpectedValueException as UnexpectedValueExceptionAlias;
use function fclose;
use function fopen;
use function is_resource;

/**
 * PhalconNG\Logger\Adapter\Stream
 *
 * Adapter to store logs in plain text files
 */
class Stream extends AbstractAdapter
{
    /**
     * Stream handler resource
     *
     * @var resource|null|false
     */
    protected $handler = null;

    /**
     * The file open mode. Defaults to 'ab'
     *
     * @var string
     */
    protected $mode = 'ab';

    /**
     * Path options
     *
     * @var array
     */
    protected $options;

    /**
     * Stream constructor.
     *
     * @param string $name
     * @param array  $options
     *
     * @throws Exception
     */
    public function __construct(string $name, array $options = [])
    {
        /**
         * Mode
         */
        $mode = null;
        if (true === isset($options['mode'])) {
            $mode = $options['mode'];
            if (true === Str::includes('r', $options['mode'])) {
                throw new Exception('Adapter cannot be opened in read mode');
            }
        }

        if (null === $mode) {
            $mode = 'ab';
        }

        $this->name = $name;
        $this->mode = $mode;
    }

    /**
     * Closes the stream
     */
    public function close(): bool
    {
        $result = true;

        if (true === is_resource($this->handler)) {
            $result = fclose($this->handler);
        }

        $this->handler = null;

        return $result;
    }

    /**
     * Processes the message i.e. writes it to the file
     *
     * @param Item $item
     */
    public function process(Item $item): void
    {
        if (true !== is_resource($this->handler)) {
            $this->handler = fopen($this->name, $this->mode);
        }

        /** @var string  $contents */
        $contents = $this->getFormatter()->format($item);

        fwrite($this->handler, $contents);
    }
}

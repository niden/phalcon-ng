<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Logger\Adapter;

use LogicException;
use PhalconNG\Helper\Arr;
use PhalconNG\Logger\Logger;
use PhalconNG\Logger\Exception;
use PhalconNG\Logger\Item;
use function closelog;
use function syslog;
use const LOG_ERR;
use const LOG_ODELAY;
use const LOG_USER;

/**
 * PhalconNG\Logger\Adapter\Syslog
 *
 * Sends logs to the system logger
 *
 * <code>
 * use PhalconNG\Logger;
 * use PhalconNG\Logger\Adapter\Syslog;
 *
 * // LOG_USER is the only valid log type under Windows operating systems
 * $logger = new Syslog(
 *     'ident',
 *     [
 *         'option'   => LOG_CONS | LOG_NDELAY | LOG_PID,
 *         'facility' => LOG_USER,
 *     ]
 * );
 *
 * $logger->log('This is a message');
 * $logger->log(Logger::ERROR, 'This is an error');
 * $logger->error('This is another error');
 *</code>
 */
class Syslog extends AbstractAdapter
{
    /**
     * Name of the default formatter class
     *
     * @var string
     */
    protected $defaultFormatter = 'Syslog';

    /**
     * @var int
     */
    protected $facility = 0;

    /**
     * @var bool
     */
    protected $opened = false;

    /**
     * @var int
     */
    protected $option = 0;

    /**
     * Syslog constructor.
     *
     * @param string $name
     * @param array  $options
     */
    public function __construct(string $name, array $options = [])
    {
        $this->name     = $name;
        $this->facility = Arr::get($options, 'facility', LOG_USER);
        $this->option   = Arr::get($options, 'option', LOG_ODELAY);
    }

    /**
     * Closes the logger
     */
    public function close(): bool
    {
        if (true !== $this->opened) {
            return true;
        }

        return closelog();
    }

    /**
     * Processes the message i.e. writes it to the syslog
     *
     * @param Item $item
     *
     * @throws Exception
     */
    public function process(Item $item): void
    {
        $formatter = $this->getFormatter();
        $message   = $formatter->format($item);

        if (true !== is_array($message)) {
            throw new Exception('The formatted message is not valid');
        }

        $result = openlog($this->name, $this->option, $this->facility);
        if (false === $result) {
            throw new LogicException(
                sprintf(
                    'Cannot open syslog for name [%s] and facility [%s]',
                    $this->name,
                    $this->facility
                )
            );
        }

        $this->opened = true;
        syslog($this->logLevelToSyslog($message[1]), $message[1]);
    }

    /**
     * Translates a Logger level to a Syslog level
     *
     * @param string $level
     *
     * @return int
     */
    private function logLevelToSyslog(string $level): int
    {
        $levels = [
            Logger::ALERT     => LOG_ALERT,
            Logger::CRITICAL  => LOG_CRIT,
            Logger::CUSTOM    => LOG_ERR,
            Logger::DEBUG     => LOG_DEBUG,
            Logger::EMERGENCY => LOG_EMERG,
            Logger::ERROR     => LOG_ERR,
            Logger::INFO      => LOG_INFO,
            Logger::NOTICE    => LOG_NOTICE,
            Logger::WARNING   => LOG_WARNING,
        ];

        return Arr::get($levels, $level, LOG_ERR);
    }
}

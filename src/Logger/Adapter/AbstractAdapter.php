<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Logger\Adapter;

use PhalconNG\Logger\Exception;
use PhalconNG\Logger\Formatter\FormatterInterface;
use PhalconNG\Logger\Item;
use function is_object;

/**
 * Class AbstractAdapter
 *
 * @package PhalconNG\Logger\Adapter
 */
abstract class AbstractAdapter implements AdapterInterface
{
    /**
     * Name of the default formatter class
     *
     * @var string
     */
    protected $defaultFormatter = 'Line';

    /**
     * Formatter
     *
     * @var FormatterInterface
     */
    protected $formatter;

    /**
     * Tells if there is an active transaction or not
     *
     * @var bool
     */
    protected $inTransaction = false;

    /**
     * Stream name
     *
     * @var string
     */
    protected $name;

    /**
     * Array with messages queued in the transaction
     *
     * @var array
     */
    protected $queue = [];

    /**
     * Destructor cleanup
     *
     * @throws Exception
     */
    public function __destruct()
    {
        if (true === $this->inTransaction) {
            $this->commit();
        }

        $this->close();
    }

    /**
     * Commits the internal transaction
     *
     * @return AdapterInterface
     * @throws Exception
     */
    public function commit(): AdapterInterface
    {
        if (true !== $this->inTransaction) {
            throw new Exception('There is no active transaction');
        }

        /**
         * Check if the queue has something to log
         */
        foreach ($this->queue as $item) {
            $this->process($item);
        }

        // Clear logger queue at commit
        $this->inTransaction = false;
        $this->queue         = [];

        return $this;
    }

    /**
     * Processes the message in the adapter
     *
     * @param Item $item
     *
     * @throws Exception
     */
    abstract public function process(Item $item): void;

    /**
     * Adds a message to the queue
     *
     * @param Item $item
     *
     * @return
     */
    public function add(Item $item): AdapterInterface
    {
        $this->queue[] = $item;

        return $this;
    }

    /**
     * Starts a transaction
     *
     * @return AdapterInterface
     */
    public function begin(): AdapterInterface
    {
        $this->inTransaction = true;

        return $this;
    }

    /**
     * Returns the formatter - if not set, it sets it
     *
     * @return FormatterInterface
     */
    public function getFormatter(): FormatterInterface
    {
        if (true !== is_object($this->formatter)) {
            $className       = sprintf('PhalconNG\Logger\Formatter\%s', $this->defaultFormatter);
            $this->formatter = new $className();
        }

        return $this->formatter;
    }

    /**
     * Sets the message formatter
     *
     * @param FormatterInterface $formatter
     *
     * @return AdapterInterface
     */
    public function setFormatter(FormatterInterface $formatter): AdapterInterface
    {
        $this->formatter = $formatter;

        return $this;
    }

    /**
     * Returns the name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Returns the whether the logger is currently in an active transaction or
     * not
     */
    public function inTransaction(): bool
    {
        return $this->inTransaction;
    }

    /**
     * Rollbacks the internal transaction
     *
     * @return AdapterInterface
     * @throws Exception
     */
    public function rollback(): AdapterInterface
    {
        if (true !== $this->inTransaction) {
            throw new Exception('There is no active transaction');
        }

        // Clear logger queue at commit
        $this->inTransaction = false;
        $this->queue         = [];

        return $this;
    }
}

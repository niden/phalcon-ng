<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Traits;

use PhalconNG\Events\ManagerInterface;

/**
 * PhalconNG\Traits\EventsManagerTrait
 *
 * Injects the EVManager setter and getter
 */
trait EventsManagerTrait
{
    /**
     * @var ManagerInterface|null
     */
    protected $eventsManager = null;

    /**
     * Returns the internal event manager
     */
    public function getEventsManager(): ManagerInterface
    {
        return $this->eventsManager;
    }

    /**
     * Sets the internal Event Manager
     *
     * @param ManagerInterface $manager
     */
    public function setEventsManager(ManagerInterface $manager): void
    {
        $this->eventsManager = $manager;
    }
}

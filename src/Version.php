<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG;

use function sprintf;

/**
 * PhalconNG\Version
 *
 * This class allows to get the installed version of the framework
 */
class Version
{

    /**
     * The constant referencing the major version. Returns 0
     *
     * <code>
     * echo PhalconNG\Version::getPart(
     *     PhalconNG\Version::VERSION_MAJOR
     * );
     * </code>
     */
    const VERSION_MAJOR = 0;

    /**
     * The constant referencing the major version. Returns 1
     *
     * <code>
     * echo PhalconNG\Version::getPart(
     *     PhalconNG\Version::VERSION_MEDIUM
     * );
     * </code>
     */
    const VERSION_MEDIUM = 1;

    /**
     * The constant referencing the major version. Returns 2
     *
     * <code>
     * echo PhalconNG\Version::getPart(
     *     PhalconNG\Version::VERSION_MINOR
     * );
     * </code>
     */
    const VERSION_MINOR = 2;

    /**
     * The constant referencing the major version. Returns 3
     *
     * <code>
     * echo PhalconNG\Version::getPart(
     *     PhalconNG\Version::VERSION_SPECIAL
     * );
     * </code>
     */
    const VERSION_SPECIAL = 3;

    /**
     * The constant referencing the major version. Returns 4
     *
     * <code>
     * echo PhalconNG\Version::getPart(
     *     PhalconNG\Version::VERSION_SPECIAL_NUMBER
     * );
     * </code>
     */
    const VERSION_SPECIAL_NUMBER = 4;

    /**
     * Returns the numeric active version
     *
     * @return string
     */
    public static function getId(): string
    {
        $version = self::getVersion();

        return $version[self::VERSION_MAJOR] .
            sprintf("%02s", $version[self::VERSION_MEDIUM]) .
            sprintf("%02s", $version[self::VERSION_MINOR]) .
            $version[self::VERSION_SPECIAL] .
            $version[self::VERSION_SPECIAL_NUMBER];
    }

    /**
     * Area where the version number is set. The format is as follows:
     * ABBCCDE
     *
     * A - Major version
     * B - Med version (two digits)
     * C - Min version (two digits)
     * D - Special release: 1 = alpha, 2 = beta, 3 = RC, 4 = stable
     * E - Special release version i.e. RC1, Beta2 etc.
     */
    final protected static function getVersion(): array
    {
        return [4, 0, 0, 1, 5];
    }

    /**
     * Returns a specific part of the version. If the wrong parameter is passed
     * it will return the full version
     *
     * @param int $part
     *
     * @return string
     */
    public static function getPart(int $part): string
    {
        $version = self::getVersion();

        switch ($part) {
            case self::VERSION_MAJOR:
            case self::VERSION_MEDIUM:
            case self::VERSION_MINOR:
            case self::VERSION_SPECIAL_NUMBER:
                return (string) $version[$part];

            case self::VERSION_SPECIAL:
                return (string) self::getSpecial(
                    $version[self::VERSION_SPECIAL]
                );

            default:
                return self::get();
        }
    }

    /**
     * Translates a number to a special release.
     *
     * @param int $special
     *
     * @return string
     */
    final protected static function getSpecial(int $special): string
    {
        $suffix = "";

        switch ($special) {
            case 1:
                $suffix = "alpha";
                break;
            case 2:
                $suffix = "beta";
                break;
            case 3:
                $suffix = "RC";
                break;
        }

        return $suffix;
    }

    /**
     * Returns the active version (string)
     *
     * <code>
     * echo PhalconNG\Version::get();
     * </code>
     */
    public static function get(): string
    {
        $version = self::getVersion();
        $result  = $version[self::VERSION_MAJOR] . "."
            . $version[self::VERSION_MEDIUM] . "."
            . $version[self::VERSION_MINOR];
        $suffix  = self::getSpecial($version[self::VERSION_SPECIAL]);

        if ($suffix !== "") {
            /**
             * A pre-release version should be denoted by appending a hyphen and
             * a series of dot separated identifiers immediately following the
             * patch version.
             */
            $result .= "-" . $suffix;
            if ($version[self::VERSION_SPECIAL_NUMBER] !== 0) {
                $result .= "." . $version[self::VERSION_SPECIAL_NUMBER];
            }
        }

        return $result;
    }
}

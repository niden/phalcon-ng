<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Events;

/**
 * PhalconNG\Events\EventsAwareInterface
 *
 * This interface must for those classes that accept an EventsManager and
 * dispatch events
 */
interface EventsAwareInterface
{
    /**
     * Returns the internal event manager
     */
    public function getEventsManager(): ManagerInterface;

    /**
     * Sets the events manager
     *
     * @param ManagerInterface $eventsManager
     */
    public function setEventsManager(ManagerInterface $eventsManager): void;
}

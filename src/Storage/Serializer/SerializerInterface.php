<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Storage\Serializer;

use Serializable;

/**
 * Interface SerializerInterface
 *
 * @package PhalconNG\Storage\Serializer
 */
interface SerializerInterface extends Serializable
{
    /**
     * @var mixed
     */
    public function getData();

    /**
     * @param $data
     */
    public function setData($data): void;
}

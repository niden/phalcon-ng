<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Storage\Serializer;

use function igbinary_serialize;
use function igbinary_unserialize;

/**
 * Class Igbinary
 *
 * @package PhalconNG\Storage\Serializer
 */
class Igbinary extends AbstractSerializer
{
    /**
     * Serializes data
     *
     * @return string|null
     */
    public function serialize()
    {
        if ($this->isSerializable($this->data)) {
            return $this->data;
        }

        return igbinary_serialize($this->data);
    }

    /**
     * Unserializes data
     *
     * @param string $data
     */
    public function unserialize($data): void
    {
        $this->data = igbinary_unserialize($data);
    }
}

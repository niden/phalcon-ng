<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Storage\Adapter;

use APCUIterator;
use PhalconNG\Storage\Exception;
use PhalconNG\Storage\SerializerFactory;
use function apcu_dec;
use function apcu_delete;
use function apcu_exists;
use function apcu_fetch;
use function apcu_inc;
use function apcu_store;
use function is_object;
use function mb_strtolower;

/**
 * PhalconNG\Storage\Adapter\Apcu
 *
 * Apcu adapter
 */
class Apcu extends AbstractAdapter
{
    /**
     * @var array
     */
    protected $options = [];

    /**
     * Apcu constructor.
     *
     * @param array $options
     */
    /**
     * Apcu constructor.
     *
     * @param SerializerFactory $factory
     * @param array             $options
     */
    public function __construct(SerializerFactory $factory, array $options = [])
    {
        /**
         * Lets set some defaults and options here
         */
        $this->prefix  = "ph-apcu-";
        $this->options = $options;

        parent::__construct($factory, $options);

        $this->initSerializer();
    }

    /**
     * Flushes/clears the cache
     */
    public function clear(): bool
    {
        $pattern = "/^" . $this->prefix . "/";
        $apc     = new APCUIterator($pattern);
        $result  = true;

        if (!is_object($apc)) {
            return false;
        }

        foreach ($apc as $item) {
            if (!apcu_delete($item["key"])) {
                $result = false;
            }
        }

        return $result;
    }

    /**
     * Decrements a stored number
     *
     * @param string $key
     * @param int    $value
     *
     * @return bool|int
     */
    public function decrement(string $key, int $value = 1)
    {
        return apcu_dec($this->getPrefixedKey($key), $value);
    }

    /**
     * Reads data from the adapter
     *
     * @param string $key
     *
     * @return bool
     */
    public function delete(string $key): bool
    {
        return apcu_delete($this->getPrefixedKey($key));
    }

    /**
     * Reads data from the adapter
     *
     * @param string $key
     * @param null   $defaultValue
     *
     * @return mixed
     */
    public function get(string $key, $defaultValue = null)
    {
        $content = apcu_fetch($this->getPrefixedKey($key));

        return $this->getUnserializedData($content, $defaultValue);
    }

    /**
     * Returns the already connected adapter or connects to the Memcached
     * server(s)
     *
     * @return mixed
     */
    public function getAdapter()
    {
        return $this->adapter;
    }

    /**
     * Stores data in the adapter
     *
     * @return array
     */
    public function getKeys(): array
    {
        $pattern = "/^" . $this->prefix . "/";
        $apc     = new APCUIterator($pattern);
        $results = [];

        if (!is_object($apc)) {
            return $results;
        }

        foreach ($apc as $item) {
            $results[] = $item['key'];
        }

        return $results;
    }

    /**
     * Checks if an element exists in the cache
     *
     * @param string $key
     *
     * @return bool
     */
    public function has(string $key): bool
    {
        return apcu_exists($this->getPrefixedKey($key));
    }

    /**
     * Increments a stored number
     *
     * @param string $key
     * @param int    $value
     *
     * @return bool|int
     */
    public function increment(string $key, int $value = 1)
    {
        return apcu_inc($this->getPrefixedKey($key), $value);
    }

    /**
     * Stores data in the adapter
     *
     * @param string $key
     * @param mixed  $value
     * @param null   $ttl
     *
     * @return bool
     * @throws Exception
     */
    public function set(string $key, $value, $ttl = null): bool
    {
        return apcu_store(
            $this->getPrefixedKey($key),
            $this->getSerializedData($value),
            $this->getTtl($ttl)
        );
    }
}

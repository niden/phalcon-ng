<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Storage\Adapter;

use PhalconNG\Collection;
use PhalconNG\Storage\SerializerFactory;

/**
 * PhalconNG\Storage\Adapter\Memory
 *
 * Memory adapter
 */
class Memory extends AbstractAdapter
{
    /**
     * @var Collection
     */
    protected $data = [];

    /**
     * @var array
     */
    protected $options = [];

    /**
     * Memory constructor.
     *
     * @param SerializerFactory $factory
     * @param array             $options
     */
    public function __construct(SerializerFactory $factory, array $options = [])
    {
        /**
         * Lets set some defaults and options here
         */
        $this->prefix  = "ph-memo-";
        $this->options = $options;
        $this->data    = new Collection();

        parent::__construct($factory, $options);

        $this->initSerializer();
    }

    /**
     * Flushes/clears the cache
     */
    public function clear(): bool
    {
        $this->data->clear();

        return true;
    }

    /**
     * Decrements a stored number
     *
     * @param string $key
     * @param int    $value
     *
     * @return bool|int
     */
    public function decrement(string $key, int $value = 1)
    {
        $prefixedKey = $this->getPrefixedKey($key);
        $result      = $this->data->has($prefixedKey);

        if ($result) {
            $current  = $this->data->get($prefixedKey);
            $newValue = (int) $current - $value;
            $result   = $newValue;

            $this->data->set($prefixedKey, $newValue);
        }

        return $result;
    }

    /**
     * Reads data from the adapter
     *
     * @param string $key
     *
     * @return bool
     */
    public function delete(string $key): bool
    {
        $prefixedKey = $this->getPrefixedKey($key);
        $exists      = $this->data->has($prefixedKey);

        $this->data->remove($prefixedKey);

        return $exists;
    }

    /**
     * Reads data from the adapter
     *
     * @param string     $key
     * @param mixed|null $defaultValue
     *
     * @return mixed
     */
    public function get(string $key, $defaultValue = null)
    {
        $prefixedKey = $this->getPrefixedKey($key);
        $content     = $this->data->get($prefixedKey);

        return $this->getUnserializedData($content, $defaultValue);
    }

    /**
     * Returns the already connected adapter or connects to the Memcached
     * server(s)
     *
     * @return mixed
     */
    public function getAdapter()
    {
        return $this->adapter;
    }

    /**
     * Stores data in the adapter
     *
     * @return array
     */
    public function getKeys(): array
    {
        return array_keys($this->data->toArray());
    }

    /**
     * Checks if an element exists in the cache
     *
     * @param string $key
     *
     * @return bool
     */
    public function has(string $key): bool
    {
        return $this->data->has($this->getPrefixedKey($key));
    }

    /**
     * Increments a stored number
     *
     * @param string $key
     * @param int    $value
     *
     * @return bool|int
     */
    public function increment(string $key, int $value = 1)
    {
        $prefixedKey = $this->getPrefixedKey($key);
        $result      = $this->data->has($prefixedKey);

        if ($result) {
            $current  = $this->data->get($prefixedKey);
            $newValue = (int) $current + $value;
            $result   = $newValue;

            $this->data->set($prefixedKey, $newValue);
        }

        return $result;
    }

    /**
     * Stores data in the adapter
     *
     * @param string $key
     * @param mixed  $value
     * @param null   $ttl
     *
     * @return bool
     */
    public function set(string $key, $value, $ttl = null): bool
    {
        $this->data->set(
            $this->getPrefixedKey($key),
            $this->getSerializedData($value)
        );

        return true;
    }
}

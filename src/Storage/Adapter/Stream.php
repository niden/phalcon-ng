<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Storage\Adapter;

use PhalconNG\Helper\Arr;
use PhalconNG\Helper\Str;
use PhalconNG\Storage\Exception;
use function file_exists;
use PhalconNG\Storage\SerializerFactory;
use function time;

/**
 * PhalconNG\Storage\Adapter\Stream
 *
 * Stream adapter
 */
class Stream extends AbstractAdapter
{
    /**
     * @var string
     */
    protected $cacheDir = "";

    /**
     * @var array
     */
    protected $options = [];

    /**
     * Stream constructor.
     *
     * @param SerializerFactory $factory
     * @param array             $options
     *
     * @throws Exception
     */
    public function __construct(SerializerFactory $factory, array $options = [])
    {
        $cacheDir = Arr::get($options, "cacheDir", "");
        if (empty($cacheDir)) {
            throw new Exception("The 'cacheDir' must be specified in the options");
        }

        /**
         * Lets set some defaults and options here
         */
        $this->cacheDir = rtrim($cacheDir, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
        $this->prefix   = "phstrm-";
        $this->options  = $options;

        parent::__construct($factory, $options);

        $this->initSerializer();
    }

    /**
     * Flushes/clears the cache
     */
    public function clear(): bool
    {
        $result    = true;
        $directory = rtrim($this->cacheDir . $this->prefix, DIRECTORY_SEPARATOR);
        $iterator  = $this->rglob($directory . "/*");

        foreach ($iterator as $file) {
            if (!is_dir($file) && !unlink($file)) {
                $result = false;
            }
        }

        return $result;
    }

    /**
     * @param string $pattern
     *
     * @return array
     */
    private function rglob(string $pattern): array
    {
        $dirName = dirname($pattern) . "/*";
        $files   = glob($pattern);
        $flags   = GLOB_ONLYDIR | GLOB_NOSORT;

        foreach (glob($dirName, $flags) as $dir) {
            $dir     = $dir . "/" . basename($pattern);
            $recurse = $this->rglob($dir);
            $files   = array_merge($files, $recurse);
        }

        return $files;
    }

    /**
     * Decrements a stored number
     *
     * @param string $key
     * @param int    $value
     *
     * @return bool|int
     * @throws \Exception
     */
    public function decrement(string $key, int $value = 1)
    {
        if (!$this->has($key)) {
            return false;
        }

        $data = $this->get($key);
        $data = (int) $data - $value;

        return $this->set($key, $data);
    }

    /**
     * Checks if an element exists in the cache and is not expired
     *
     * @param string $key
     *
     * @return bool
     */
    public function has(string $key): bool
    {
        $directory = $this->getDir($key);
        $exists    = file_exists($directory . $key);

        if (!$exists) {
            return false;
        }

        $payload = file_get_contents($directory . $key);
        $payload = json_decode($payload, true);

        return !$this->isExpired($payload);
    }

    /**
     * Returns the folder based on the cacheDir and the prefix
     *
     * @param string $key
     *
     * @return string
     */
    private function getDir(string $key = ""): string
    {
        $directory     = rtrim($this->cacheDir . $this->prefix, DIRECTORY_SEPARATOR);
        $fileDirectory = rtrim(Str::folderFromFile($key), DIRECTORY_SEPARATOR);

        return $directory . DIRECTORY_SEPARATOR . $fileDirectory . DIRECTORY_SEPARATOR;
    }

    /**
     * Returns if the cache has expired for this item or not
     *
     * @param array $payload
     *
     * @return bool
     */
    private function isExpired(array $payload): bool
    {
        $created = Arr::get($payload, "created", time());
        $ttl     = Arr::get($payload, "ttl", 3600);

        return ($created + $ttl) < time();
    }

    /**
     * Reads data from the adapter
     *
     * @param string $key
     * @param null   $defaultValue
     *
     * @return mixed|null
     */
    public function get(string $key, $defaultValue = null)
    {
        $directory = $this->getDir($key);
        if (!file_exists($directory . $key)) {
            return $defaultValue;
        }

        $payload = file_get_contents($directory . $key);
        $payload = json_decode($payload, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            return $defaultValue;
        }

        if ($this->isExpired($payload)) {
            return $defaultValue;
        }

        $content = Arr::get($payload, "content", null);

        return $this->getUnserializedData($content, $defaultValue);
    }

    /**
     * Stores data in the adapter
     *
     * @param string $key
     * @param mixed  $value
     * @param null   $ttl
     *
     * @return bool
     * @throws \Exception
     */
    public function set(string $key, $value, $ttl = null): bool
    {
        $payload   = [
            "created" => time(),
            "ttl"     => $this->getTtl($ttl),
            "content" => $this->getSerializedData($value),
        ];
        $payload   = json_encode($payload);
        $directory = $this->getDir($key);

        if (!is_dir($directory)) {
            mkdir($directory, 0777, true);
        }

        return false !== file_put_contents($directory . $key, $payload);
    }

    /**
     * Reads data from the adapter
     *
     * @param string $key
     *
     * @return bool
     */
    public function delete(string $key): bool
    {
        if (!$this->has($key)) {
            return false;
        }

        $directory = $this->getDir($key);

        return unlink($directory . $key);
    }

    /**
     * Returns the already connected adapter or connects to the Memcached
     * server(s)
     */
    public function getAdapter()
    {
        return $this->adapter;
    }

    /**
     * Stores data in the adapter
     */
    public function getKeys(): array
    {
        $results   = [];
        $directory = rtrim($this->cacheDir . $this->prefix, DIRECTORY_SEPARATOR);
        $iterator  = $this->rglob($directory . "/*");

        foreach ($iterator as $file) {
            if (!is_dir($file)) {
                $split     = explode("/", $file);
                $results[] = $this->prefix . Arr::last($split);
            }
        }

        return $results;
    }

    /**
     * Increments a stored number
     *
     * @param string $key
     * @param int    $value
     *
     * @return bool|int
     * @throws \Exception
     */
    public function increment(string $key, int $value = 1)
    {
        if (!$this->has($key)) {
            return false;
        }

        $data = $this->get($key);
        $data = (int) $data + $value;

        return $this->set($key, $data);
    }
}

<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Storage\Adapter;

use DateInterval;
use DateTime;
use Exception;
use function mb_strtolower;
use PhalconNG\Helper\Arr;
use PhalconNG\Storage\Serializer\SerializerInterface;
use PhalconNG\Storage\SerializerFactory;

/**
 * Class AbstractAdapter
 *
 * @package PhalconNG\Storage\Adapter
 */
abstract class AbstractAdapter implements AdapterInterface
{
    /**
     * @var mixed
     */
    protected $adapter;

    /**
     * Name of the default serializer class
     *
     * @var string
     */
    protected $defaultSerializer = "Php";

    /**
     * Name of the default TTL (time to live)
     *
     * @var int
     */
    protected $lifetime = 3600;

    /**
     * @var string
     */
    protected $prefix = "";

    /**
     * Serializer
     *
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @var SerializerFactory
     */
    protected $serializerFactory;

    /**
     * AbstractAdapter constructor.
     *
     * @param SerializerFactory $factory
     * @param array             $options
     */
    protected function __construct(SerializerFactory $factory, array $options)
    {
        /**
         * Lets set some defaults and options here
         */
        $this->defaultSerializer = Arr::get($options, "defaultSerializer", "Php");
        $this->lifetime          = Arr::get($options, "lifetime", 3600);
        $this->serializer        = Arr::get($options, "serializer", null);
        $this->serializerFactory = $factory;

        if (isset($options["prefix"])) {
            $this->prefix = $options["prefix"];
        }
    }

    /**
     * Flushes/clears the cache
     */
    abstract public function clear(): bool;

    /**
     * Decrements a stored number
     *
     * @param string $key
     * @param int    $value
     *
     * @return int|bool
     */
    abstract public function decrement(string $key, int $value = 1);

    /**
     * Deletes data from the adapter
     *
     * @param string $key
     *
     * @return bool
     */
    abstract public function delete(string $key): bool;

    /**
     * Reads data from the adapter
     *
     * @param string $key
     *
     * @return mixed
     */
    abstract public function get(string $key);

    /**
     * Returns the adapter - connects to the storage if not connected
     *
     * @return mixed
     */
    abstract public function getAdapter();

    /**
     * Returns all the keys stored
     *
     * @return array
     */
    abstract public function getKeys(): array;

    /**
     * @return string
     */
    public function getPrefix(): string
    {
        return $this->prefix;
    }

    /**
     * Checks if an element exists in the cache
     *
     * @param string $key
     *
     * @return bool
     */
    abstract public function has(string $key): bool;

    /**
     * Increments a stored number
     *
     * @param string $key
     * @param int    $value
     *
     * @return bool|int
     */
    abstract public function increment(string $key, int $value = 1);

    /**
     * Stores data in the adapter
     *
     * @param string $key
     * @param mixed  $value
     * @param null   $ttl
     *
     * @return bool
     */
    abstract public function set(string $key, $value, $ttl = null): bool;

    /**
     * Returns the key requested, prefixed
     *
     * @param $key
     *
     * @return string
     */
    protected function getPrefixedKey($key): string
    {
        $key = (string) $key;

        return $this->prefix . $key;
    }

    /**
     * Returns serialized data
     *
     * @param mixed $content
     *
     * @return mixed
     */
    protected function getSerializedData($content)
    {
        if ('' !== $this->defaultSerializer) {
            $this->serializer->setData($content);
            $content = $this->serializer->serialize();
        }

        return $content;
    }

    /**
     * Calculates the TTL for a cache item
     *
     * @param DateInterval|int|null $ttl
     *
     * @return int
     * @throws Exception
     */
    protected function getTtl($ttl = null): int
    {
        if (null === $ttl) {
            return $this->lifetime;
        }

        if (is_object($ttl) && $ttl instanceof DateInterval) {
            $dateTime = new DateTime("@0");
            return $dateTime->add($ttl)->getTimestamp();
        }

        return (int) $ttl;
    }

    /**
     * Returns unserialized data
     */
    /**
     * @param mixed $content
     * @param null  $defaultValue
     *
     * @return mixed
     */
    protected function getUnserializedData($content, $defaultValue = null)
    {
        if (false === $content) {
            return $defaultValue;
        }

        if ('' !== $this->defaultSerializer) {
            $this->serializer->unserialize($content);
            $content = $this->serializer->getData();
        }

        return $content;
    }

    protected function initSerializer()
    {
        $className        = mb_strtolower($this->defaultSerializer);
        $this->serializer = $this->serializerFactory->newInstance($className);
    }
}

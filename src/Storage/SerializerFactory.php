<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Storage;

use PhalconNG\Storage\Serializer\Base64;
use PhalconNG\Storage\Serializer\Igbinary;
use PhalconNG\Storage\Serializer\Json;
use PhalconNG\Storage\Serializer\Msgpack;
use PhalconNG\Storage\Serializer\None;
use PhalconNG\Storage\Serializer\Php;
use function array_merge;

class SerializerFactory
{
    /**
     * @var array
     */
    private $mapper = [];

    /**
     * @var array
     */
    private $services = [];

    /**
     * SerializerFactory constructor.
     *
     * @param array $services
     */
    public function __construct(array $services = [])
    {
        $helpers = [
            'base64'   => function () {
                return new Base64();
            },
            'igbinary' => function () {
                return new Igbinary();
            },
            'json'     => function () {
                return new Json();
            },
            'msgpack'  => function () {
                return new Msgpack();
            },
            'none'     => function () {
                return new None();
            },
            'php'      => function () {
                return new Php();
            },
        ];

        $helpers = array_merge($helpers, $services);

        foreach ($helpers as $name => $service) {
            $this->mapper[$name] = $service;
            unset($this->services[$name]);
        }
    }

    /**
     * @param string $name
     *
     * @return mixed
     * @throws Exception
     */
    public function newInstance(string $name)
    {
        if (true !== isset($this->mapper[$name])) {
            throw new Exception('Service ' . $name . ' is not registered');
        }

        if (true !== isset($this->services[$name])) {
            $definition            = $this->mapper[$name];
            $this->services[$name] = $definition();
        }

        return $this->services[$name];
    }
}

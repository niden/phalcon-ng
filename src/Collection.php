<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG;

use ArrayAccess;
use ArrayIterator;
use Countable;
use IteratorAggregate;
use JsonSerializable;
use Serializable;
use Traversable;
use function mb_strtolower;

/**
 * PhalconNG\Collection
 *
 * PhalconNG\Collection is a supercharged object oriented array. It implements
 * ArrayAccess, Countable, IteratorAggregate, JsonSerializable, Serializable
 *
 * It can be used in any part of the application that needs collection of data
 * Such implementations are for instance accessing globals `$_GET`, `$_POST`
 * etc.
 */
class Collection implements ArrayAccess, Countable, IteratorAggregate, JsonSerializable, Serializable
{
    /**
     * @var array
     */
    protected $data = [];

    /**
     * @var array
     */
    protected $lowerKeys = [];

    /**
     * Collection constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->init($data);
    }

    /**
     * Initialize internal array
     *
     * @param array $data
     */
    public function init(array $data = []): void
    {
        foreach ($data as $key => $value) {
            $this->set($key, $value);
        }
    }

    /**
     * Set an $element in the collection
     *
     * @param string $element
     * @param mixed  $value
     */
    public function set(string $element, $value): void
    {
        $key = mb_strtolower($element);

        $this->data[$element]  = $value;
        $this->lowerKeys[$key] = $element;
    }

    /**
     * Magic getter to get an $element from the collection
     *
     * @param string $element
     *
     * @return mixed
     */
    public function __get(string $element)
    {
        return $this->get($element);
    }

    /**
     * Magic setter to assign values to an $element
     *
     * @param string $element
     * @param mixed  $value
     */
    public function __set(string $element, $value): void
    {
        $this->set($element, $value);
    }

    /**
     * Get the $element from the collection
     *
     * @param string      $element
     * @param mixed|null $defaultValue
     * @param bool       $insensitive
     *
     * @return mixed|null
     */
    public function get(string $element, $defaultValue = null, bool $insensitive = true)
    {
        if (true === $insensitive) {
            $element = mb_strtolower($element);
        }

        if (isset($this->lowerKeys[$element])) {
            return $this->data[$this->lowerKeys[$element]];
        }

        return $defaultValue;
    }

    /**
     * Magic isset to check whether an $element exists or not
     *
     * @param string $element
     *
     * @return bool
     */
    public function __isset(string $element): bool
    {
        return $this->has($element);
    }

    /**
     * Get the $element from the collection
     *
     * @param string $element
     * @param bool   $insensitive
     *
     * @return bool
     */
    public function has(string $element, bool $insensitive = true): bool
    {
        if (true === $insensitive) {
            $element = mb_strtolower($element);
        }

        return isset($this->lowerKeys[$element]);
    }

    /**
     * Magic unset to remove an $element from the collection
     *
     * @param string $element
     */
    public function __unset(string $element): void
    {
        $this->remove($element);
    }

    /**
     * Delete the $element from the collection
     *
     * @param string $element
     * @param bool   $insensitive
     */
    public function remove(string $element, bool $insensitive = true): void
    {
        if (true === $this->has($element, $insensitive)) {
            if (true === $insensitive) {
                $element = mb_strtolower($element);
            }

            $key = $this->lowerKeys[$element];

            unset($this->data[$key]);
            unset($this->lowerKeys[$element]);
        }
    }

    /**
     * Clears the internal collection
     */
    public function clear(): void
    {
        $this->data      = [];
        $this->lowerKeys = [];
    }

    /**
     * Count $elements of an object
     *
     * @link https://php.net/manual/en/countable.count.php
     */
    public function count(): int
    {
        return count($this->data);
    }

    /**
     * Returns the iterator of the class
     */
    public function getIterator(): Traversable
    {
        return new ArrayIterator($this);
    }

    /**
     * Specify data which should be serialized to JSON
     *
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     */
    public function jsonSerialize(): array
    {
        return $this->data;
    }

    /**
     * Whether a offset exists
     *
     * @param mixed $element
     *
     * @return bool
     *
     * @link https://php.net/manual/en/arrayaccess.offsetexists.php
     */
    public function offsetExists($element): bool
    {
        $element = (string) $element;

        return $this->has($element);
    }

    /**
     * Offset to retrieve
     *
     *
     * @param mixed $element
     *
     * @return mixed|null
     *
     * @link https://php.net/manual/en/arrayaccess.offsetget.php
     */
    public function offsetGet($element)
    {
        $element = (string) $element;

        return $this->get($element);
    }

    /**
     * Offset to set
     *
     * @param mixed $element
     * @param mixed $value
     *
     * @link https://php.net/manual/en/arrayaccess.offsetset.php
     */
    public function offsetSet($element, $value): void
    {
        $element = (string) $element;

        $this->set($element, $value);
    }

    /**
     * Offset to unset
     *
     * @param mixed $element
     *
     * @link https://php.net/manual/en/arrayaccess.offsetunset.php
     */
    public function offsetUnset($element): void
    {
        $element = (string) $element;

        $this->remove($element);
    }

    /**
     * String representation of object
     *
     * @return string
     *
     * @link https://php.net/manual/en/serializable.serialize.php
     */
    public function serialize(): string
    {
        return serialize($this->data);
    }

    /**
     * Returns the object in an array format
     */
    public function toArray(): array
    {
        return $this->data;
    }

    /**
     * Returns the object in a JSON format
     *
     * @param int $options The default options for json_encode are:
     *                     JSON_HEX_TAG, JSON_HEX_APOS, JSON_HEX_AMP,
     *                     JSON_HEX_QUOT, JSON_UNESCAPED_SLASHES
     *
     * @return string
     *
     * @see https://www.ietf.org/rfc/rfc4627.txt
     */
    public function toJson(int $options = 79): string
    {
        return json_encode($this->data, $options);
    }

    /**
     * Constructs the object
     *
     * @param string $serialized
     *
     * @link https://php.net/manual/en/serializable.unserialize.php
     */
    public function unserialize($serialized): void
    {
        $serialized = (string) $serialized;
        $data       = unserialize($serialized);

        $this->init($data);
    }
}

<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Filter\Sanitize;

use function strip_tags;

/**
 * PhalconNG\Filter\Sanitize\Striptags
 *
 * Sanitizes a value striptags
 */
class Striptags
{
    /**
     * @param mixed $input The text to sanitize
     *
     * @return string
     */
    public function __invoke($input): string
    {
        return strip_tags($input);
    }
}

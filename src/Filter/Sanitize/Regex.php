<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Filter\Sanitize;

use function preg_replace;

/**
 * PhalconNG\Filter\Sanitize\Regex
 *
 * Sanitizes a value performing preg_replace
 */
class Regex
{
    /**
     * @param mixed $input The text to sanitize
     * @param       $pattern
     * @param       $replace
     *
     * @return string|string[]|null
     */
    public function __invoke($input, $pattern, $replace)
    {
        return preg_replace($pattern, $replace, $input);
    }
}

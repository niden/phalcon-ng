<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Filter\Sanitize;

/**
 * PhalconNG\Filter\Sanitize\Trim
 *
 * Sanitizes a value removing leading and trailing spaces
 */
class Trim
{
    /**
     * @param mixed $input The text to sanitize
     *
     * @return string
     */
    public function __invoke($input): string
    {
        return \trim($input);
    }
}

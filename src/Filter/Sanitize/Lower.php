<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Filter\Sanitize;

use function mb_convert_case;
use const MB_CASE_LOWER;

/**
 * PhalconNG\Filter\Sanitize\Lower
 *
 * Sanitizes a value to lowercase
 */
class Lower
{
    /**
     * @param mixed $input The text to sanitize
     *
     * @return string
     */
    public function __invoke($input): string
    {
        return mb_convert_case($input, MB_CASE_LOWER, "UTF-8");
    }
}

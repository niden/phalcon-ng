<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Filter\Sanitize;

use function array_key_exists;

/**
 * PhalconNG\Filter\Sanitize\BoolVal
 *
 * Sanitizes a value to boolean
 */
class BoolVal
{
    /**
     * @param mixed $input The text to sanitize
     *
     * @return bool
     */
    public function __invoke($input): bool
    {
        $trueArray  = [
            "true" => 1,
            "on"   => 1,
            "yes"  => 1,
            "y"    => 1,
            "1"    => 1,
        ];
        $falseArray = [
            "false" => 1,
            "off"   => 1,
            "no"    => 1,
            "n"     => 1,
            "0"     => 1,
        ];

        if (array_key_exists($input, $trueArray)) {
            return true;
        }

        if (array_key_exists($input, $falseArray)) {
            return false;
        }

        return (bool) $input;
    }
}

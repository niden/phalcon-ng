<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Filter\Sanitize;

use function str_replace;

/**
 * PhalconNG\Filter\Sanitize\Remove
 *
 * Sanitizes a value removing parts of a string
 */
class Remove
{
    /**
     * @param mixed $input The text to sanitize
     * @param       $replace
     *
     * @return mixed
     */
    public function __invoke($input, $replace)
    {
        return str_replace($replace, "", $input);
    }
}

<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Filter\Sanitize;

use function preg_replace;

/**
 * PhalconNG\Filter\Sanitize\Alnum
 *
 * Sanitizes a value to an alphanumeric value
 */
class Alnum
{
    /**
     * @param mixed $input The text to sanitize
     *
     * @return string
     */
    public function __invoke($input): string
    {
        return preg_replace("/[^A-Za-z0-9]/", "", $input);
    }
}

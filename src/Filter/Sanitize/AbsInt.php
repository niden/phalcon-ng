<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Filter\Sanitize;

/**
 * PhalconNG\Filter\Sanitize\AbsInt
 *
 * Sanitizes a value to absolute integer
 */
class AbsInt
{
    /**
     * @param mixed $input The text to sanitize
     *
     * @return int|float
     */
    public function __invoke($input)
    {
        return \abs(\intval(\filter_var($input, FILTER_SANITIZE_NUMBER_INT)));
    }
}

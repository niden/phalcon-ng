<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Filter\Sanitize;

use function mb_convert_case;
use const MB_CASE_UPPER;

/**
 * PhalconNG\Filter\Sanitize\Upper
 *
 * Sanitizes a value to uppercase
 */
class Upper
{
    /**
     * @param mixed $input The text to sanitize
     *
     * @return string
     */
    public function __invoke($input): string
    {
        return mb_convert_case($input, MB_CASE_UPPER, "UTF-8");
    }
}

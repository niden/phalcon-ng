<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Filter\Sanitize;

use function str_replace;

/**
 * PhalconNG\Filter\Sanitize\Replace
 *
 * Sanitizes a value replacing parts of a string
 */
class Replace
{
    /**
     * @param mixed $input The text to sanitize
     * @param mixed $from
     * @param mixed $to
     *
     * @return mixed
     */
    public function __invoke($input, $from, $to)
    {
        return str_replace($from, $to, $input);
    }
}

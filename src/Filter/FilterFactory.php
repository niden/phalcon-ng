<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Filter;

use PhalconNG\Filter\Sanitize\AbsInt;
use PhalconNG\Filter\Sanitize\Alnum;
use PhalconNG\Filter\Sanitize\Alpha;
use PhalconNG\Filter\Sanitize\BoolVal;
use PhalconNG\Filter\Sanitize\Email;
use PhalconNG\Filter\Sanitize\FloatVal;
use PhalconNG\Filter\Sanitize\IntVal;
use PhalconNG\Filter\Sanitize\Lower;
use PhalconNG\Filter\Sanitize\LowerFirst;
use PhalconNG\Filter\Sanitize\Regex;
use PhalconNG\Filter\Sanitize\Remove;
use PhalconNG\Filter\Sanitize\Replace;
use PhalconNG\Filter\Sanitize\Special;
use PhalconNG\Filter\Sanitize\SpecialFull;
use PhalconNG\Filter\Sanitize\StringVal;
use PhalconNG\Filter\Sanitize\Striptags;
use PhalconNG\Filter\Sanitize\Trim;
use PhalconNG\Filter\Sanitize\Upper;
use PhalconNG\Filter\Sanitize\UpperFirst;
use PhalconNG\Filter\Sanitize\UpperWords;
use PhalconNG\Filter\Sanitize\Url;
use function array_merge;

/**
 * Class FilterFactory
 *
 * @package PhalconNG\Filter
 */
class FilterFactory
{
    const FILTER_ABSINT      = 'absint';
    const FILTER_ALNUM       = 'alnum';
    const FILTER_ALPHA       = 'alpha';
    const FILTER_BOOL        = 'bool';
    const FILTER_EMAIL       = 'email';
    const FILTER_FLOAT       = 'float';
    const FILTER_INT         = 'int';
    const FILTER_LOWER       = 'lower';
    const FILTER_LOWERFIRST  = 'lowerFirst';
    const FILTER_REGEX       = 'regex';
    const FILTER_REMOVE      = 'remove';
    const FILTER_REPLACE     = 'replace';
    const FILTER_SPECIAL     = 'special';
    const FILTER_SPECIALFULL = 'specialFull';
    const FILTER_STRING      = 'string';
    const FILTER_STRIPTAGS   = 'striptags';
    const FILTER_TRIM        = 'trim';
    const FILTER_UPPER       = 'upper';
    const FILTER_UPPERFIRST  = 'upperFirst';
    const FILTER_UPPERWORDS  = 'upperWords';
    const FILTER_URL         = 'url';

    /**
     * @var array
     */
    private $mapper = [];

    /**
     * @var array
     */
    private $services = [];

    /**
     * SerializerFactory constructor.
     *
     * @param array $services
     */
    public function __construct(array $services = [])
    {
        $helpers = [
            self::FILTER_ABSINT      => function () {
                return new AbsInt();
            },
            self::FILTER_ALNUM       => function () {
                return new Alnum();
            },
            self::FILTER_ALPHA       => function () {
                return new Alpha();
            },
            self::FILTER_BOOL        => function () {
                return new BoolVal();
            },
            self::FILTER_EMAIL       => function () {
                return new Email();
            },
            self::FILTER_FLOAT       => function () {
                return new FloatVal();
            },
            self::FILTER_INT         => function () {
                return new IntVal();
            },
            self::FILTER_LOWER       => function () {
                return new Lower();
            },
            self::FILTER_LOWERFIRST  => function () {
                return new LowerFirst();
            },
            self::FILTER_REGEX       => function () {
                return new Regex();
            },
            self::FILTER_REMOVE      => function () {
                return new Remove();
            },
            self::FILTER_REPLACE     => function () {
                return new Replace();
            },
            self::FILTER_SPECIAL     => function () {
                return new Special();
            },
            self::FILTER_SPECIALFULL => function () {
                return new SpecialFull();
            },
            self::FILTER_STRING      => function () {
                return new StringVal();
            },
            self::FILTER_STRIPTAGS   => function () {
                return new Striptags();
            },
            self::FILTER_TRIM        => function () {
                return new Trim();
            },
            self::FILTER_UPPER       => function () {
                return new Upper();
            },
            self::FILTER_UPPERFIRST  => function () {
                return new UpperFirst();
            },
            self::FILTER_UPPERWORDS  => function () {
                return new UpperWords();
            },
            self::FILTER_URL         => function () {
                return new Url();
            },
        ];

        $helpers = array_merge($helpers, $services);

        foreach ($helpers as $name => $service) {
            $this->mapper[$name] = $service;
            unset($this->services[$name]);
        }
    }

    /**
     * @param string $name
     *
     * @return mixed
     * @throws Exception
     */
    public function newInstance(string $name)
    {
        if (true !== isset($this->mapper[$name])) {
            throw new Exception('Service ' . $name . ' is not registered');
        }

        if (true !== isset($this->services[$name])) {
            $definition            = $this->mapper[$name];
            $this->services[$name] = $definition();
        }

        return $this->services[$name];
    }
}

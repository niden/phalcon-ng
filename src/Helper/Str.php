<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon.
 *
 * (c) Phalcon Team <team@phalcon.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PhalconNG\Helper;

use RuntimeException;
use function array_filter;
use function array_merge;
use function count;
use function count_chars;
use function explode;
use function implode;
use function mb_convert_case;
use function mb_strlen;
use function mb_strpos;
use function mb_strstr;
use function mb_strtolower;
use function mb_substr;
use function mt_rand;
use function pathinfo;
use function preg_match_all;
use function preg_replace;
use function range;
use function str_replace;
use function str_split;
use function strcasecmp;
use function strcmp;
use function strncmp;
use function strrev;
use function trim;
use const MB_CASE_LOWER;
use const MB_CASE_TITLE;
use const MB_CASE_UPPER;

/**
 * PhalconNG\Helper\Str
 *
 * This class offers quick string functions through the framework
 */
class Str
{
    const RANDOM_ALNUM    = 0;
    const RANDOM_ALPHA    = 1;
    const RANDOM_DISTINCT = 5;
    const RANDOM_HEXDEC   = 2;
    const RANDOM_NOZERO   = 4;
    const RANDOM_NUMERIC  = 3;

    /**
     * Converts strings to camelize style
     *
     * @param string      $text
     * @param string|null $delimiter
     * @param string      $encoding
     *
     * @return string
     */
    final public static function camelize(string $text, string $delimiter = null, $encoding = 'UTF-8'): string
    {
        $from = ['_', '-', '.'];
        if (null !== $delimiter) {
            $from = array_merge($from, str_split($delimiter));
        }

        return str_replace(
            ' ',
            '',
            mb_convert_case(
                str_replace($from, ' ', $text),
                MB_CASE_TITLE,
                $encoding
            )
        );
    }

    /**
     * Concatenates strings using the separator only once without duplication in
     * places concatenation
     *
     * @param string $delimiter
     * @param mixed  ...$arguments
     *
     * @return string
     */
    final public static function concat(string $delimiter, ...$arguments): string
    {
        if (count($arguments) < 2) {
            throw new RuntimeException('concat needs at least three parameters');
        }

        $first  = Arr::first($arguments);
        $last   = Arr::last($arguments);
        $prefix = '';
        $suffix = '';
        $data   = [];

        if (true === self::startsWith($first, $delimiter)) {
            $prefix = $delimiter;
        }

        if (true === self::endsWith($last, $delimiter)) {
            $suffix = $delimiter;
        }


        foreach ($arguments as $argument) {
            $data[] = trim($argument, $delimiter);
        }

        return $prefix . implode($delimiter, $data) . $suffix;
    }

    /**
     * Check if a string starts with a given string
     *
     * @param string $text
     * @param string $start
     * @param bool   $ignoreCase
     *
     * @return bool
     */
    final public static function startsWith(string $text, string $start, bool $ignoreCase = true): bool
    {
        if (true === empty($text)) {
            return false;
        }

        if (true === $ignoreCase) {
            $text  = mb_strtolower($text);
            $start = mb_strtolower($start);
        }

        return strncmp($text, $start, mb_strlen($start)) === 0;
    }

    /**
     * Check if a string ends with a given string
     *
     * @param string $text
     * @param string $end
     * @param bool   $ignoreCase
     *
     * @return bool
     */
    final public static function endsWith(string $text, string $end, bool $ignoreCase = true): bool
    {
        if (true === empty($text)) {
            return false;
        }

        $chunk = mb_substr($text, -(mb_strlen($end)));

        if (true === $ignoreCase) {
            return (bool) (0 === strcasecmp($chunk, $end));
        } else {
            return (bool) (0 === strcmp($chunk, $end));
        }
    }

    /**
     * Returns number of vowels in provided string. Uses a regular expression
     * to count the number of vowels (A, E, I, O, U) in a string.
     *
     * @param string $text
     *
     * @return int
     */
    final public static function countVowels(string $text): int
    {
        preg_match_all('/[aeiou]/i', $text, $matches);

        return count($matches[0]);
    }

    /**
     * Decapitalizes the first letter of the sring and then adds it with rest
     * of the string. Omit the upperRest parameter to keep the rest of the
     * string intact, or set it to true to convert to uppercase.
     *
     * @param string $text
     * @param bool   $upperRest
     * @param string $encoding
     *
     * @return string
     */
    final public static function decapitalize(
        string $text,
        bool $upperRest = false,
        string $encoding = 'UTF-8'
    ): string {
        $substr = mb_substr($text, 1);

        if (true === $upperRest) {
            $suffix = mb_convert_case($substr, MB_CASE_UPPER, $encoding);
        } else {
            $suffix = $substr;
        }

        return mb_convert_case(
            mb_substr($text, 0, 1),
            MB_CASE_LOWER,
            $encoding
        ) . $suffix;
    }

    /**
     * Returns the first string there is between the strings from the
     * parameter start and end.
     *
     * @param string $text
     * @param string $start
     * @param string $end
     *
     * @return string
     */
    final public static function firstBetween(
        string $text,
        string $start,
        string $end
    ): string {
        return trim(
            mb_strstr(
                mb_strstr($text, $start),
                $end,
                true
            ),
            $start . $end
        );
    }

    /**
     * Accepts a file name (without extension) and returns a calculated folder
     * structure with the filename in the end
     *
     * @param string $file
     *
     * @return string
     */
    final public static function folderFromFile(string $file): string
    {
        $name  = pathinfo($file, PATHINFO_FILENAME);
        $start = mb_substr($name, 0, -2);

        if (true === empty($start)) {
            $start = mb_substr($name, 0, 1);
        }

        return implode('/', str_split($start, 2)) . '/';
    }

    /**
     * Makes an underscored or dashed phrase human-readable
     *
     * @param string $text
     *
     * @return string
     */
    final public static function humanize(string $text): string
    {
        return preg_replace('#[_-]+#', ' ', trim($text));
    }

    /**
     * Lets you determine whether or not a string includes another string.
     *
     * @param string $needle
     * @param string $haystack
     *
     * @return bool
     */
    final public static function includes(string $needle, string $haystack): bool
    {
        return !(false === mb_strpos($haystack, $needle));
    }

    /**
     * Adds a number to a string or increment that number if it already is
     * defined
     *
     * @param string $text
     * @param string $separator
     *
     * @return string
     */
    final public static function increment(string $text, string $separator = '_'): string
    {
        $parts = explode($separator, $text);
        if (true === isset($parts[1])) {
            $number = $parts[1];
            $number++;
        } else {
            $number = 1;
        }

        return $parts[0] . $separator . $number;
    }

    /**
     * Compare two strings and returns true if both strings are anagram,
     * false otherwise.
     *
     * @param string $first
     * @param string $second
     *
     * @return bool
     */
    final public static function isAnagram(string $first, string $second): bool
    {
        return count_chars($first, 1) === count_chars($second, 1);
    }

    /**
     * Returns true if the given string is lower case, false otherwise.
     *
     * @param string $text
     * @param string $encoding
     *
     * @return bool
     */
    final public static function isLower(string $text, string $encoding = 'UTF-8'): bool
    {
        return $text === mb_convert_case($text, MB_CASE_LOWER, $encoding);
    }

    /**
     * Returns true if the given string is a palindrome, false otherwise.
     *
     * @param string $text
     *
     * @return bool
     */
    final public static function isPalindrome(string $text): bool
    {
        return strrev($text) === $text;
    }

    /**
     * Returns true if the given string is upper case, false otherwise.
     *
     * @param string $text
     * @param string $encoding
     *
     * @return bool
     */
    final public static function isUpper(string $text, string $encoding = 'UTF-8'): bool
    {
        return $text === mb_convert_case($text, MB_CASE_UPPER, $encoding);
    }

    /**
     * Lowercases a string, this function makes use of the mbstring extension if
     * available
     *
     * @param string $text
     * @param string $encoding
     *
     * @return string
     */
    final public static function lower(string $text, string $encoding = 'UTF-8'): string
    {
        return mb_convert_case($text, MB_CASE_LOWER, $encoding);
    }

    /**
     * Generates a random string based on the given type. Type is one of the
     * RANDOM_* constants
     *
     * @param int $type
     * @param int $length
     *
     * @return string
     */
    final public static function random(int $type = 0, int $length = 8): string
    {
        switch ($type) {
            case self::RANDOM_ALPHA:
                $pool = array_merge(range('a', 'z'), range('A', 'Z'));
                break;

            case self::RANDOM_HEXDEC:
                $pool = array_merge(range(0, 9), range('a', 'f'));
                break;

            case self::RANDOM_NUMERIC:
                $pool = range(0, 9);
                break;

            case self::RANDOM_NOZERO:
                $pool = range(1, 9);
                break;

            case self::RANDOM_DISTINCT:
                $pool = str_split('2345679ACDEFHJKLMNPRSTUVWXYZ');
                break;

            default:
                // Default type \PhalconNG\Text::RANDOM_ALNUM
                $pool = array_merge(
                    range(0, 9),
                    range('a', 'z'),
                    range('A', 'Z')
                );

                break;
        }

        $end  = count($pool) - 1;
        $text = '';
        while (mb_strlen($text) < $length) {
            $text .= $pool[mt_rand(0, $end)];
        }

        return $text;
    }

    /**
     * Reduces multiple slashes in a string to single slashes
     *
     * @param string $text
     *
     * @return string
     */
    final public static function reduceSlashes(string $text): string
    {
        return preg_replace('#(?<!:)//+#', '/', $text);
    }

    /**
     * Uncamelize strings which are camelized
     *
     * @param string      $text
     * @param string|null $delimiter
     * @param string      $encoding
     *
     * @return string
     */
    final public static function uncamelize(string $text, string $delimiter = null, $encoding = 'UTF-8'): string
    {
        $from = ['_', '-', '.'];
        if (null !== $delimiter) {
            $from = array_merge($from, str_split($delimiter));
        }
        $chunks = explode(' ', str_replace($from, ' ', $text));
        $chunks = array_filter($chunks, 'lcfirst');

        return implode(' ', $chunks);
    }

    /**
     * Makes a phrase underscored instead of spaced
     *
     * @param string $text
     *
     * @return string
     */
    final public static function underscore(string $text): string
    {
        return preg_replace('#\s+#', '_', trim($text));
    }

    /**
     * Uppercases a string, this function makes use of the mbstring extension if
     * available
     *
     * @param string $text
     * @param string $encoding
     *
     * @return string
     */
    final public static function upper(string $text, string $encoding = 'UTF-8'): string
    {
        return mb_convert_case($text, MB_CASE_UPPER, $encoding);
    }
}

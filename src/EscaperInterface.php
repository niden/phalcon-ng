<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG;

/**
 * PhalconNG\EscaperInterface
 *
 * Interface for PhalconNG\Escaper
 */
interface EscaperInterface
{
    /**
     * Escape CSS strings by replacing non-alphanumeric chars by their
     * hexadecimal representation
     *
     * @param string $css
     *
     * @return string
     */
    public function escapeCss(string $css): string;

    /**
     * Escapes a HTML string
     *
     * @param string $text
     *
     * @return string
     */
    public function escapeHtml(string $text): string;

    /**
     * Escapes a HTML attribute string
     *
     * @param string $text
     *
     * @return string
     */
    public function escapeHtmlAttr(string $text): string;

    /**
     * Escape Javascript strings by replacing non-alphanumeric chars by their
     * hexadecimal representation
     *
     * @param string $js
     *
     * @return string
     */
    public function escapeJs(string $js): string;

    /**
     * Escapes a URL. Internally uses rawurlencode
     *
     * @param string $url
     *
     * @return string
     */
    public function escapeUrl(string $url): string;

    /**
     * Returns the internal encoding used by the escaper
     *
     * @return string
     */
    public function getEncoding(): string;

    /**
     * Sets the encoding to be used by the escaper
     *
     * @param string $encoding
     */
    public function setEncoding(string $encoding): void;

    /**
     * Sets the HTML quoting type for htmlspecialchars
     *
     * @param int $quoteType
     */
    public function setHtmlQuoteType(int $quoteType): void;
}

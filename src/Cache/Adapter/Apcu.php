<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Cache\Adapter;

use PhalconNG\Cache\Adapter\AdapterInterface as CacheAdapterInterface;
use PhalconNG\Storage\Adapter\Apcu as StorageApcu;

/**
 * PhalconNG\Cache\Adapter\Apcu
 *
 * Apcu adapter
 */
class Apcu extends StorageApcu implements CacheAdapterInterface
{
}

<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Cache;

use PhalconNG\Cache\Adapter\AdapterInterface;
use PhalconNG\Cache\Exception\Exception;

/**
 * PhalconNG\CacheFactory
 *
 * Creates a new Cache class
 */
class CacheFactory
{
    /**
     * @var AdapterFactory
     */
    private $adapterFactory;

    /**
     * CacheFactory constructor.
     *
     * @param AdapterFactory $factory
     */
    public function __construct(AdapterFactory $factory)
    {
        $this->adapterFactory = $factory;
    }

    /**
     * @param string $name
     * @param array  $options
     *
     * @return Cache
     * @throws Exception
     */
    public function newInstance(string $name, array $options = []): Cache
    {
        /** @var AdapterInterface $adapter */
        $adapter = $this->adapterFactory->newInstance($name, $options);

        return new Cache($adapter);
    }
}

<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Cache;

use function array_merge;
use PhalconNG\Cache\Exception\Exception;
use PhalconNG\Cache\Adapter\Apcu;
use PhalconNG\Cache\Adapter\Libmemcached;
use PhalconNG\Cache\Adapter\Memory;
use PhalconNG\Cache\Adapter\Redis;
use PhalconNG\Cache\Adapter\Stream;
use PhalconNG\Storage\SerializerFactory;

class AdapterFactory
{
    /**
     * @var array
     */
    private $mapper   = [];

    /**
     * @var array
     */
    private $services = [];

    /**
     * @var SerializerFactory
     */
    private $serializerFactory;

    /**
     * AdapterFactory constructor.
     *
     * @param SerializerFactory $factory
     * @param array             $services
     */
    public function __construct(SerializerFactory $factory, array $services = [])
    {
        $this->serializerFactory = $factory;

        $helpers = [
            'apcu'         => function ($factory, $options = []) {
                return new Apcu($factory, $options);
            },
            'libmemcached' => function ($factory, $options = []) {
                return new Libmemcached($factory, $options);
            },
            'memory'       => function ($factory, $options = []) {
                return new Memory($factory, $options);
            },
            'redis'        => function ($factory, $options = []) {
                return new Redis($factory, $options);
            },
            'stream'       => function ($factory, $options = []) {
                return new Stream($factory, $options);
            },
        ];

        $helpers = array_merge($helpers, $services);

        foreach ($helpers as $name => $service) {
            $this->mapper[$name] = $service;
            unset($this->services[$name]);
        }
    }

    /**
     * @param string $name
     * @param array  $options
     *
     * @return Cache
     * @throws Exception
     */
    public function newInstance(string $name, array $options = []): Cache
    {
        if (true !== isset($this->mapper[$name])) {
            throw new Exception('Service ' . $name . ' is not registered');
        }

        if (true !== isset($this->services[$name])) {
            $definition            = $this->mapper[$name];
            $this->services[$name] = $definition($this->serializerFactory, $options);
        }

        return $this->services[$name];
    }
}

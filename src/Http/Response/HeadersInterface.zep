
/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Http\Response;

/**
 * PhalconNG\Http\Response\HeadersInterface
 *
 * Interface for PhalconNG\Http\Response\Headers compatible bags
 */
interface HeadersInterface
{
    /**
     * Restore a \PhalconNG\Http\Response\Headers object
     */
    public static function __set_state(array! data) -> <HeadersInterface>;

    /**
     * Gets a header value from the internal bag
     */
    public function get(string name) -> string | bool;

    /**
     * Returns true if the header is set, false otherwise
     */
    public function has(string name) -> bool;

    /**
     * Reset set headers
     */
    public function reset();

    /**
     * Sends the headers to the client
     */
    public function send() -> bool;

    /**
     * Sets a header to be sent at the end of the request
     */
    public function set(string name, string value);

    /**
     * Sets a raw header to be sent at the end of the request
     */
    public function setRaw(string header);
}

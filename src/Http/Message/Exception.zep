
/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Http\Message;

/**
 * PhalconNG\Http\Message\Exception
 *
 * Exceptions thrown in PhalconNG\Http\Message/* will use this class
 *
 */
class Exception extends \PhalconNG\Exception
{

}

<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG;

use function htmlspecialchars;
use function mb_convert_encoding;
use function mb_detect_encoding;
use function rawurlencode;

/**
 * PhalconNG\Escaper
 *
 * Escapes different kinds of text securing them. By using this component you
 * may prevent XSS attacks.
 */
class Escaper implements EscaperInterface
{
    /**
     * @var bool
     */
    protected $doubleEncode = true;

    /**
     * @var string
     */
    protected $encoding = 'utf-8';

    protected $htmlEscapeMap = null;

    protected $htmlQuoteType = 3;

    /**
     * Escape CSS strings by replacing non-alphanumeric chars by their
     * hexadecimal escaped representation
     *
     * @param string $css
     *
     * @return string
     */
    public function escapeCss(string $css): string
    {
//        /**
//         * Normalize encoding to UTF-32
//         * Escape the string
//         */
//        return phalcon_escape_css(this->normalizeEncoding(css));
    }

    /**
     * Escape javascript strings by replacing non-alphanumeric chars by their
     * hexadecimal escaped representation
     *
     * @param string $js
     *
     * @return string
     */
    public function escapeJs(string $js): string
    {
//        /**
//         * Normalize encoding to UTF-32
//         * Escape the string
//         */
//        return phalcon_escape_js(this->normalizeEncoding(js));
    }

    /**
     * Escapes a HTML string. Internally uses htmlspecialchars
     *
     * @param string $text
     *
     * @return string
     */
    public function escapeHtml(string $text): string
    {
        return htmlspecialchars(
            $text,
            $this->htmlQuoteType,
            $this->encoding,
            $this->doubleEncode
        );
    }

    /**
     * Escapes a HTML attribute string
     *
     * @param string $attribute
     *
     * @return string
     */
    public function escapeHtmlAttr(string $attribute): string
    {
        return htmlspecialchars(
            $attribute,
            ENT_QUOTES,
            $this->encoding,
            $this->doubleEncode
        );
    }

    /**
     * Escapes a URL. Internally uses rawurlencode
     *
     * @param string $url
     *
     * @return string
     */
    public function escapeUrl(string $url): string
    {
        return rawurlencode($url);
    }

    /**
     * Returns the internal encoding used by the escaper
     */
    public function getEncoding(): string
    {
        return $this->encoding;
    }

    /**
     * Sets the encoding to be used by the escaper
     *
     * @param string $encoding
     */
    public function setEncoding(string $encoding): void
    {
        $this->encoding = $encoding;
    }

    /**
     * Utility to normalize a string's encoding to UTF-32
     * .
     *
     * @param string $text
     *
     * @return string
     */
    final public function normalizeEncoding(string $text): string
    {
        /**
         * Convert to UTF-32 (4 byte characters, regardless of actual number of
         * bytes in the character).
         */
        return mb_convert_encoding(
            $text,
            'UTF-32',
            $this->detectEncoding($text)
        );
    }

    /**
     * Detect the character encoding of a string to be handled by an encoder.
     * Special-handling for chr(172) and chr(128) to chr(159) which fail to be
     * detected by mb_detect_encoding()
     *
     * @param string $text
     *
     * @return false|string|null
     */
    final public function detectEncoding(string $text)
    {
        $charsets = ['UTF-32', 'UTF-8', 'ISO-8859-1', 'ASCII'];

        /**
         * Strict encoding detection with fallback to non-strict detection.
         * Check encoding
         */
        foreach ($charsets as $charset) {
            if (true === mb_detect_encoding($text, $charset, true)) {
                return $charset;
            }
        }

        /**
         * Fallback to global detection
         */
        return mb_detect_encoding($text);
    }

    /**
     * Sets the double_encode to be used by the escaper
     *
     * @param bool $doubleEncode
     */
    public function setDoubleEncode(bool $doubleEncode): void
    {
        $this->doubleEncode = $doubleEncode;
    }

    /**
     * Sets the HTML quoting type for htmlspecialchars
     *
     * @param int $quoteType
     */
    public function setHtmlQuoteType(int $quoteType): void
    {
        $this->htmlQuoteType = $quoteType;
    }
}

<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Html;

use PhalconNG\Escaper;
use PhalconNG\EscaperInterface;
use PhalconNG\Html\Helper\Anchor;
use PhalconNG\Html\Helper\AnchorRaw;
use PhalconNG\Html\Helper\Body;
use PhalconNG\Html\Helper\Button;
use PhalconNG\Html\Helper\Close;
use PhalconNG\Html\Helper\Element;
use PhalconNG\Html\Helper\ElementRaw;
use PhalconNG\Html\Helper\Form;
use PhalconNG\Html\Helper\Img;
use PhalconNG\Html\Helper\Label;
use PhalconNG\Html\Helper\TextArea;
use function array_merge;

/**
 * PhalconNG\Html\TagLocator
 *
 * ServiceLocator implementation for Tag helpers
 */
class TagFactory
{
    /**
     * @var EscaperInterface
     */
    private $escaper;

    /**
     * @var array
     */
    private $mapper = [];

    /**
     * @var array
     */
    private $services = [];

    /**
     * SerializerFactory constructor.
     *
     * @param array $services
     */
    /**
     * TagFactory constructor.
     *
     * @param Escaper $escaper
     * @param array   $services
     */
    public function __construct(Escaper $escaper, array $services = [])
    {
        $this->escaper = $escaper;

        /**
         * Available helpers
         */
        $helpers = [
            'a'          => function ($escaper) {
                return new Anchor($escaper);
            },
            'aRaw'       => function ($escaper) {
                return new AnchorRaw($escaper);
            },
            'body'       => function ($escaper) {
                return new Body($escaper);
            },
            'button'     => function ($escaper) {
                return new Button($escaper);
            },
            'close'      => function ($escaper) {
                return new Close($escaper);
            },
            //            'doctype'    => function ($escaper) {
            //                return null;
            //            },
            'element'    => function ($escaper) {
                return new Element($escaper);
            },
            'elementRaw' => function ($escaper) {
                return new ElementRaw($escaper);
            },
            'form'       => function ($escaper) {
                return new Form($escaper);
            },
            //            'head'       => function ($escaper) {
            //                return null;
            //            },
            'img'        => function ($escaper) {
                return new Img($escaper);
            },
            //            'input'      => function ($escaper) {
            //                return null;
            //            },
            'label'      => function ($escaper) {
                return new Label($escaper);
            },
            //            'link'       => function ($escaper) {
            //                return null;
            //            },
            //            'script'     => function ($escaper) {
            //                return null;
            //            },
            //            'style'      => function ($escaper) {
            //                return null;
            //            },
            'textarea'   => function ($escaper) {
                return new TextArea($escaper);
            },
            //            'title'      => function ($escaper) {
            //                return null;
            //            },
        ];

        $helpers = array_merge($helpers, $services);

        foreach ($helpers as $name => $service) {
            $this->mapper[$name] = $service;
            unset($this->services[$name]);
        }
    }

    /**
     * @param string $name
     *
     * @return mixed
     * @throws Exception
     */
    public function newInstance(string $name)
    {
        if (true !== isset($this->mapper[$name])) {
            throw new Exception('Service ' . $name . ' is not registered');
        }

        if (true !== isset($this->services[$name])) {
            $definition            = $this->mapper[$name];
            $this->services[$name] = $definition($this->escaper);
        }

        return $this->services[$name];
    }
}

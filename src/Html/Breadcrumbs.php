<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Html;

use PhalconNG\Helper\Arr;
use function count;
use function str_replace;

/**
 * PhalconNG\Html\Breadcrumbs
 *
 * This component offers an easy way to create breadcrumbs for your application.
 * The resulting HTML when calling `render()` will have each breadcrumb enclosed
 * in `<dt>` tags, while the whole string is enclosed in `<dl>` tags.
 */
class Breadcrumbs
{
    /**
     * Keeps all the breadcrumbs
     *
     * @var array
     */
    private $elements = [];

    /**
     * Crumb separator
     *
     * @var string
     */
    private $separator = ' / ';

    /**
     * The HTML template to use to render the breadcrumbs.
     *
     * @var string
     */
    private $template = '<dt><a href="%link%">%label%</a></dt>';

    /**
     * Adds a new crumb.
     *
     * @param string $label
     * @param string $link
     *
     * @return Breadcrumbs
     */
    public function add(string $label, string $link = ''): Breadcrumbs
    {
        $this->elements[$link] = $label;

        return $this;
    }

    /**
     * Clears the crumbx
     */
    public function clear(): void
    {
        $this->elements = [];
    }

    /**
     * Returns the separator
     *
     * @return string
     */
    public function getSeparator(): string
    {
        return $this->separator;
    }

    /**
     * Returns the separator
     *
     * @param string $separator
     *
     * @return Breadcrumbs
     */
    public function setSeparator(string $separator): Breadcrumbs
    {
        $this->separator = $separator;

        return $this;
    }

    /**
     * Removes crumb by url.
     *
     * @param string $link
     */
    public function remove(string $link): void
    {
        unset($this->elements[$link]);
    }

    /**
     * Renders and outputs breadcrumbs based on previously set template.
     *
     * <code>
     * // Php Engine
     * echo $breadcrumbs->render();
     * </code>
     */
    public function render(): string
    {
        $elements  = $this->elements;
        $lastUrl   = Arr::lastKey($elements);
        $lastLabel = Arr::last($elements);
        $output    = [];

        unset($elements[$lastUrl]);

        foreach ($elements as $url => $element) {
            $output[] = str_replace(
                [
                    '%label%',
                    '%link%',
                ],
                [
                    $element,
                    $url,
                ],
                $this->template
            );
        }

        /**
         * Check if this is the 'Home' element i.e. count() = 0
         */
        if (0 !== count($elements)) {
            $output[] = '<dt>' . $lastLabel . '</dt>';
        } else {
            $output[] = str_replace(
                [
                    '%label%',
                    '%link%',
                ],
                [
                    $lastLabel,
                    $lastUrl,
                ],
                $this->template
            );
        }

        return '<dl>' . implode('<dt>' . $this->separator . '</dt>', $output) . '</dl>';
    }

    /**
     * Returns the internal breadcrumbs array
     */
    public function toArray(): array
    {
        return $this->elements;
    }
}

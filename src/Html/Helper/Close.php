<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Html\Helper;

/**
 * PhalconNG\Html\Helper\FormClose
 *
 * Creates a form closing tag
 */
class Close extends AbstractHelper
{
    /**
     * @param string $tag The tag for the anchor
     *
     * @return string
     */
    public function __invoke(string $tag): string
    {
        return '</' . $tag . '>';
    }
}

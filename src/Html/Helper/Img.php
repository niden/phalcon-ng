<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Html\Helper;

use PhalconNG\Html\Exception;
use function array_merge;

/**
 * PhalconNG\Html\Helper\Img
 *
 * Creates am img tag
 */
class Img extends AbstractHelper
{
    /**
     * @param string $src
     * @param array  $attributes Any additional attributes
     *
     * @return string
     * @throws Exception
     */
    public function __invoke(string $src, array $attributes = []): string
    {
        $overrides = ['src' => $src];

        /**
         * Avoid duplicate "src" and ignore it if it is passed in the attributes
         */
        unset($attributes['src']);

        $overrides = array_merge($overrides, $attributes);

        return $this->selfClose('img', $overrides);
    }
}

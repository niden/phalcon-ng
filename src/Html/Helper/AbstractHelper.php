<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Html\Helper;

use PhalconNG\EscaperInterface;
use PhalconNG\Html\Exception;
use function array_intersect_key;
use function array_merge;
use function gettype;
use function is_object;
use function is_resource;
use function is_string;

/**
 * PhalconNG\Html\Helper\AbstractHelper
 *
 * Abstract class for all html helpers
 */
abstract class AbstractHelper
{
    /**
     * @var EscaperInterface
     */
    protected $escaper;

    /**
     * AbstractHelper constructor.
     *
     * @param EscaperInterface $escaper
     */
    public function __construct(EscaperInterface $escaper)
    {
        $this->escaper = $escaper;
    }

    /**
     * Renders an element
     *
     * @param string $tag
     * @param string $text
     * @param array  $attributes
     * @param bool   $raw
     *
     * @return string
     * @throws Exception
     */
    protected function renderFullElement(
        string $tag,
        string $text,
        array $attributes = [],
        bool $raw = false
    ): string {
        $content = (true === $raw) ? $text : $this->escaper->escapeHtml($text);

        return $this->renderElement($tag, $attributes) . $content . '</' . $tag . '>';
    }

    /**
     * Renders an element
     *
     * @param string $tag
     * @param array  $attributes
     *
     * @return string
     * @throws Exception
     */
    protected function renderElement(string $tag, array $attributes = []): string
    {
        $escapedAttrs = '';
        if (count($attributes) > 0) {
            $attrs        = $this->orderAttributes([], $attributes);
            $escapedAttrs = ' ' . rtrim($this->renderAttributes($attrs));
        }

        return '<' . $tag . $escapedAttrs . '>';
    }

    /**
     * Keeps all the attributes sorted - same order all the tome
     *
     * @param array $overrides
     * @param array $attributes
     *
     * @return array
     */
    protected function orderAttributes(array $overrides, array $attributes): array
    {
        $order = [
            'rel'    => null,
            'type'   => null,
            'for'    => null,
            'src'    => null,
            'href'   => null,
            'action' => null,
            'id'     => null,
            'name'   => null,
            'value'  => null,
            'class'  => null,
        ];

        $intersect = array_intersect_key($order, $attributes);
        $results   = array_merge($intersect, $attributes);
        $results   = array_merge($overrides, $results);

        /**
         * Just in case remove the 'escape' attribute
         */
        unset($results['escape']);

        return $results;
    }

    /**
     * Renders all the attributes
     *
     * @param array $attributes
     *
     * @return string
     * @throws Exception
     */
    protected function renderAttributes(array $attributes): string
    {
        $result = '';
        foreach ($attributes as $key => $value) {
            if (true === is_string($key) && null !== $value) {
                if (true === is_array($value) || true === is_resource($value) || true === is_object($value)) {
                    throw new Exception(
                        'Value at index: "' . $key . '" type: "' . gettype($value) . '" cannot be rendered'
                    );
                }

                $result .= $key . '="' . $this->escaper->escapeHtmlAttr($value) . '" ';
            }
        }

        return $result;
    }

    /**
     * Produces a self close tag i.e. <img />
     *
     * @param string $tag
     * @param array  $attributes
     *
     * @return string
     * @throws Exception
     */
    protected function selfClose(string $tag, array $attributes = []): string
    {
        $escapedAttrs = '';

        if (count($attributes) > 0) {
            $attrs        = $this->orderAttributes([], $attributes);
            $escapedAttrs = ' ' . rtrim($this->renderAttributes($attrs));
        }

        return '<' . $tag . $escapedAttrs . '/>';
    }
}

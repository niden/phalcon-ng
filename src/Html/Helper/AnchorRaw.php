<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Html\Helper;

use PhalconNG\Html\Exception;
use function array_merge;

/**
 * PhalconNG\Html\Helper\AnchorRaw
 *
 * Creates an anchor raw
 */
class AnchorRaw extends AbstractHelper
{
    /**
     * @param string $href       The URL for the button
     * @param string $text       The text for the anchor
     * @param array  $attributes Any additional attributes
     *
     * @return string
     * @throws Exception
     */
    public function __invoke(string $href, string $text, array $attributes = []): string
    {
        $overrides = ['href' => $href];

        /**
         * Avoid duplicate "href" and ignore it if it is passed in the attributes
         */
        unset($attributes['href']);

        $overrides = array_merge($overrides, $attributes);

        return $this->renderFullElement('a', $text, $overrides, true);
    }
}

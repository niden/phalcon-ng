<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Html\Helper;

use PhalconNG\Html\Exception;

/**
 * PhalconNG\Html\Helper\TextArea
 *
 * Creates a textarea tag
 */
class TextArea extends AbstractHelper
{
    /**
     * @param string $text       The text for the anchor
     * @param array  $attributes Any additional attributes
     *
     * @return string
     * @throws Exception
     */
    public function __invoke(string $text, array $attributes = []): string
    {
        return $this->renderFullElement('textarea', $text, $attributes);
    }
}

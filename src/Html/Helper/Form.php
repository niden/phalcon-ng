<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Html\Helper;

use PhalconNG\Html\Exception;
use function array_merge;

/**
 * PhalconNG\Html\Helper\Form
 *
 * Creates a form opening tag
 */
class Form extends AbstractHelper
{
    /**
     * @param array $attributes Any additional attributes
     *
     * @return string
     * @throws Exception
     */
    public function __invoke(array $attributes = []): string
    {
        $overrides = [
            'method'  => 'post',
            'enctype' => 'multipart/form-data',
        ];

        $overrides = array_merge($overrides, $attributes);

        return $this->renderElement('form', $overrides);
    }
}

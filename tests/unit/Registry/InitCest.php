<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Registry;

use PhalconNG\Registry;
use UnitTester;

/**
 * Class InitCest
 */
class InitCest
{
    /**
     * Tests PhalconNG\Registry :: init()
     *
     * @param UnitTester $I
     */
    public function collectionInit(UnitTester $I)
    {
        $I->wantToTest('Registry - init()');
        $data = [
            'one'   => 'two',
            'three' => 'four',
            'five'  => 'six',
        ];

        $registry = new Registry();

        $expected = 0;
        $actual   = $registry->count();
        $I->assertEquals($expected, $actual);

        $registry->init($data);
        $expected = $data;
        $actual   = $registry->toArray();
        $I->assertEquals($expected, $actual);
    }
}

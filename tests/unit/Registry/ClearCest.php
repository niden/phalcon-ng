<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Registry;

use PhalconNG\Registry;
use UnitTester;

/**
 * Class ClearCest
 */
class ClearCest
{
    /**
     * Tests PhalconNG\Registry :: clear()
     *
     * @param UnitTester $I
     */
    public function collectionClear(UnitTester $I)
    {
        $I->wantToTest('Registry - clear()');
        $data     = [
            'one'   => 'two',
            'three' => 'four',
            'five'  => 'six',
        ];
        $registry = new Registry($data);

        $expected = $data;
        $actual   = $registry->toArray();
        $I->assertEquals($expected, $actual);

        $registry->clear();
        $expected = 0;
        $actual   = $registry->count();
        $I->assertEquals($expected, $actual);
    }
}

<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Registry;

use PhalconNG\Registry;
use UnitTester;

/**
 * Class GetCest
 */
class GetCest
{
    /**
     * Tests PhalconNG\Registry :: get()
     *
     * @param UnitTester $I
     */
    public function collectionGet(UnitTester $I)
    {
        $I->wantToTest('Registry - get()');
        $data     = [
            'one'   => 'two',
            'three' => 'four',
            'five'  => 'six',
        ];
        $registry = new Registry($data);

        $expected = 'four';
        $actual   = $registry->get('three');
        $I->assertEquals($expected, $actual);

        $actual = $registry->get('unknown', 'four');
        $I->assertEquals($expected, $actual);

        $actual = $registry['three'];
        $I->assertEquals($expected, $actual);

        $actual = $registry->three;
        $I->assertEquals($expected, $actual);

        $actual = $registry->offsetGet('three');
        $I->assertEquals($expected, $actual);
    }
}

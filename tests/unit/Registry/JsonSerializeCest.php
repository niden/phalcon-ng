<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Registry;

use PhalconNG\Registry;
use UnitTester;

/**
 * Class JsonSerializeCest
 */
class JsonSerializeCest
{
    /**
     * Tests PhalconNG\Registry :: jsonSerialize()
     *
     * @param UnitTester $I
     */
    public function collectionJsonSerialize(UnitTester $I)
    {
        $I->wantToTest('Registry - jsonSerialize()');
        $data     = [
            'one'   => 'two',
            'three' => 'four',
            'five'  => 'six',
        ];
        $registry = new Registry($data);

        $expected = $data;
        $actual   = $registry->jsonSerialize();
        $I->assertEquals($expected, $actual);
    }
}

<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Registry;

use PhalconNG\Registry;
use UnitTester;

/**
 * Class ConstructCest
 */
class ConstructCest
{
    /**
     * Tests PhalconNG\Registry :: __construct()
     *
     * @param UnitTester $I
     */
    public function collectionConstruct(UnitTester $I)
    {
        $I->wantToTest('Registry - __construct()');
        $registry = new Registry();

        $class = Registry::class;
        $I->assertInstanceOf($class, $registry);
    }
}

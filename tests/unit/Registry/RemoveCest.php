<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Registry;

use PhalconNG\Registry;
use UnitTester;

/**
 * Class RemoveCest
 */
class RemoveCest
{
    /**
     * Tests PhalconNG\Registry :: remove()
     *
     * @param UnitTester $I
     */
    public function collectionRemove(UnitTester $I)
    {
        $I->wantToTest('Registry - remove()');
        $data     = [
            'one'   => 'two',
            'three' => 'four',
            'five'  => 'six',
        ];
        $registry = new Registry($data);

        $expected = $data;
        $actual   = $registry->toArray();
        $I->assertEquals($expected, $actual);

        $registry->remove('five');
        $expected = [
            'one'   => 'two',
            'three' => 'four',
        ];
        $actual   = $registry->toArray();
        $I->assertEquals($expected, $actual);

        $registry->remove('FIVE');
        $expected = [
            'one'   => 'two',
            'three' => 'four',
        ];
        $actual   = $registry->toArray();
        $I->assertEquals($expected, $actual);

        $registry->init($data);
        unset($registry['five']);
        $expected = [
            'one'   => 'two',
            'three' => 'four',
        ];
        $actual   = $registry->toArray();
        $I->assertEquals($expected, $actual);

        $registry->init($data);
        $registry->__unset('five');
        $expected = [
            'one'   => 'two',
            'three' => 'four',
        ];
        $actual   = $registry->toArray();
        $I->assertEquals($expected, $actual);

        $registry->init($data);
        $registry->offsetUnset('five');
        $expected = [
            'one'   => 'two',
            'three' => 'four',
        ];
        $actual   = $registry->toArray();
        $I->assertEquals($expected, $actual);
    }
}

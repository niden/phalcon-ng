<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Registry;

use PhalconNG\Registry;
use UnitTester;

/**
 * Class HasCest
 */
class HasCest
{
    /**
     * Tests PhalconNG\Registry :: has()
     *
     * @param UnitTester $I
     */
    public function collectionHas(UnitTester $I)
    {
        $I->wantToTest('Registry - has()');
        $data     = [
            'one'   => 'two',
            'three' => 'four',
            'five'  => 'six',
        ];
        $registry = new Registry($data);

        $actual = $registry->has('three');
        $I->assertTrue($actual);

        $actual = $registry->has('unknown');
        $I->assertFalse($actual);

        $actual = $registry->__isset('three');
        $I->assertTrue($actual);

        $actual = isset($registry['three']);
        $I->assertTrue($actual);

        $actual = isset($registry['unknown']);
        $I->assertFalse($actual);

        $actual = $registry->offsetExists('three');
        $I->assertTrue($actual);

        $actual = $registry->offsetExists('unknown');
        $I->assertFalse($actual);
    }
}

<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Registry;

use PhalconNG\Registry;
use UnitTester;

/**
 * Class CountCest
 */
class CountCest
{
    /**
     * Tests PhalconNG\Registry :: count()
     *
     * @param UnitTester $I
     */
    public function collectionCount(UnitTester $I)
    {
        $I->wantToTest('Registry - count()');
        $data     = [
            'one'   => 'two',
            'three' => 'four',
            'five'  => 'six',
        ];
        $registry = new Registry($data);

        $I->assertCount(3, $registry->toArray());

        $actual = $registry->count();
        $I->assertEquals(3, $actual);
    }
}

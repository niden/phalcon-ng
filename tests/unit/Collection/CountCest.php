<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Collection;

use PhalconNG\Collection;
use UnitTester;

/**
 * Class CountCest
 */
class CountCest
{
    /**
     * Tests PhalconNG\Collection :: count()
     *
     * @param UnitTester $I
     *
     * @author Phalcon Team <team@phalconphp.com>
     * @since  2018-11-13
     */
    public function collectionCount(UnitTester $I)
    {
        $I->wantToTest('Collection - count()');
        $data       = [
            'one'   => 'two',
            'three' => 'four',
            'five'  => 'six',
        ];
        $collection = new Collection($data);

        $I->assertCount(3, $collection->toArray());

        $actual = $collection->count();
        $I->assertEquals(3, $actual);
    }
}

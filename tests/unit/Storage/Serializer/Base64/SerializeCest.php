<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Storage\Serializer\Base64;

use InvalidArgumentException;
use PhalconNG\Storage\Serializer\Base64;
use UnitTester;

/**
 * Class SerializeCest
 */
class SerializeCest
{
    /**
     * Tests PhalconNG\Storage\Serializer\Base64 :: serialize()
     *
     * @param UnitTester $I
     *
     * @author Phalcon Team <team@phalconphp.com>
     * @since  2019-03-30
     */
    public function storageSerializerBase64Serialize(UnitTester $I)
    {
        $I->wantToTest('Storage\Serializer\Base64 - serialize()');
        $data       = "Phalcon Framework";
        $serializer = new Base64($data);

        $expected = base64_encode($data);
        $actual   = $serializer->serialize();
        $I->assertEquals($expected, $actual);
    }

    /**
     * Tests PhalconNG\Storage\Serializer\Base64 :: serialize() - exception
     *
     * @param UnitTester $I
     *
     * @author Phalcon Team <team@phalconphp.com>
     * @since  2019-03-30
     */
    public function storageSerializerBase64SerializeException(UnitTester $I)
    {
        $I->wantToTest('Storage\Serializer\Base64 - serialize() - exception');
        $I->expectThrowable(
            new InvalidArgumentException('Data for the serializer must of type string'),
            function () {
                $serializer = new Base64(1234);
                $serializer->serialize();
            }
        );
    }
}

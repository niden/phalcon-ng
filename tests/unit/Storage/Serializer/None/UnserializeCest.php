<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Storage\Serializer\None;

use Codeception\Example;
use PhalconNG\Storage\Serializer\None;
use stdClass;
use UnitTester;

/**
 * Class UnserializeCest
 */
class UnserializeCest
{
    /**
     * Tests PhalconNG\Storage\Serializer\None :: unserialize()
     *
     * @dataProvider getExamples
     *
     * @param UnitTester $I
     * @param Example    $example
     *
     * @author       Phalcon Team <team@phalconphp.com>
     * @since        2019-03-30
     */
    public function storageSerializerNoneUnserialize(UnitTester $I, Example $example)
    {
        $I->wantToTest('Storage\Serializer\None - unserialize() - ' . $example[0]);
        $serializer = new None();
        $serialized = $example[1];
        $serializer->unserialize($serialized);

        $expected = $example[1];
        $actual   = $serializer->getData();
        $I->assertEquals($expected, $actual);
    }

    /**
     * @return array
     */
    private function getExamples(): array
    {
        return [
            [
                'integer',
                1234,
            ],
            [
                'float',
                1.234,
            ],
            [
                'string',
                'Phalcon Framework',
            ],
            [
                'array',
                ['Phalcon Framework'],
            ],
            [
                'object',
                new stdClass(),
            ],
        ];
    }
}

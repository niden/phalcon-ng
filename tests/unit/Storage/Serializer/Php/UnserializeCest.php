<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Storage\Unserializer\Php;

use Codeception\Example;
use PhalconNG\Storage\Serializer\Php;
use stdClass;
use UnitTester;

/**
 * Class UnserializeCest
 */
class UnserializeCest
{
    /**
     * Tests PhalconNG\Storage\Unserializer\Php :: unserialize()
     *
     * @dataProvider getExamples
     *
     * @param UnitTester $I
     * @param Example    $example
     *
     * @author       Phalcon Team <team@phalconphp.com>
     * @since        2019-03-30
     */
    public function storageSerializerPhpUnserialize(UnitTester $I, Example $example)
    {
        $I->wantToTest('Storage\Unserializer\Php - unserialize() - ' . $example[0]);
        $serializer = new Php();

        $expected = $example[1];
        $serializer->unserialize($example[2]);
        $actual = $serializer->getData();
        $I->assertEquals($expected, $actual);
    }

    /**
     * @return array
     */
    private function getExamples(): array
    {
        return [
            [
                'null',
                null,
                'N;',
            ],
            [
                'true',
                true,
                'b:1;',
            ],
            [
                'false',
                false,
                'b:0;',
            ],
            [
                'integer',
                1234,
                'i:1234;',
            ],
            [
                'float',
                1.234,
                'd:1.234;',
            ],
            [
                'string',
                'Phalcon Framework',
                's:17:"Phalcon Framework";',
            ],
            [
                'array',
                ['Phalcon Framework'],
                'a:1:{i:0;s:17:"Phalcon Framework";}',
            ],
            [
                'object',
                new stdClass(),
                'O:8:"stdClass":0:{}',
            ],
        ];
    }
}

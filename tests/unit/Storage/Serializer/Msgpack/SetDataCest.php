<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Storage\Serializer\Msgpack;

use PhalconNG\Storage\Serializer\Msgpack;
use UnitTester;

/**
 * Class SetDataCest
 */
class SetDataCest
{
    /**
     * Tests PhalconNG\Storage\Serializer\Msgpack :: getData()
     *
     * @param UnitTester $I
     *
     * @author Phalcon Team <team@phalconphp.com>
     * @since  2019-03-30
     */
    public function storageSerializerMsgpackSetData(UnitTester $I)
    {
        $I->wantToTest('Storage\Serializer\Msgpack - setData()');
        $data       = ["Phalcon Framework"];
        $serializer = new Msgpack();

        $actual = $serializer->getData();
        $I->assertNull($actual);

        $serializer->setData($data);

        $expected = $data;
        $actual   = $serializer->getData();
        $I->assertEquals($expected, $actual);
    }
}

<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Storage\SerializerFactory;

use Codeception\Example;
use function getOptionsLibmemcached;
use function getOptionsRedis;
use function outputDir;
use PhalconNG\Storage\Adapter\Apcu;
use PhalconNG\Storage\Adapter\Libmemcached;
use PhalconNG\Storage\Adapter\Memory;
use PhalconNG\Storage\Adapter\Redis;
use PhalconNG\Storage\Adapter\Stream;
use PhalconNG\Storage\Serializer\Base64;
use PhalconNG\Storage\Serializer\Igbinary;
use PhalconNG\Storage\Serializer\Json;
use PhalconNG\Storage\Serializer\Msgpack;
use PhalconNG\Storage\Serializer\None;
use PhalconNG\Storage\Serializer\Php;
use PhalconNG\Storage\SerializerFactory;
use PhalconNG\Storage\Exception;
use UnitTester;

/**
 * Class NewInstanceCest
 */
class NewInstanceCest
{
    /**
     * Tests PhalconNG\Storage\SerializerFactory :: newInstance()
     *
     * @dataProvider getExamples
     *
     * @param UnitTester $I
     *
     * @author Phalcon Team <team@phalconphp.com>
     * @since  2019-05-04
     *
     * @throws Exception
     */
    public function storageSerializerFactoryNewInstance(UnitTester $I, Example $example)
    {
        $I->wantToTest('Storage\SerializerFactory - newInstance() - ' . $example[0]);

        $factory = new SerializerFactory();
        $service = $factory->newInstance($example[0]);

        $class = $example[1];
        $I->assertInstanceOf($class, $service);
    }

    /**
     * Tests PhalconNG\Storage\SerializerFactory :: newInstance() - exception
     *
     * @param UnitTester $I
     *
     * @author Phalcon Team <team@phalconphp.com>
     * @since  2019-05-04
     *
     * @throws Exception
     */
    public function storageSerializerFactoryNewInstanceException(UnitTester $I)
    {
        $I->wantToTest('Storage\SerializerFactory - newInstance() - exception');

        $I->expectThrowable(
            new Exception('Service unknown is not registered'),
            function () {
                $factory = new SerializerFactory();
                $service = $factory->newInstance('unknown');
            }
        );
    }

    /**
     * @return array
     */
    private function getExamples(): array
    {
        return [
            ['base64',   Base64::class],
            ['igbinary', Igbinary::class],
            ['json',     Json::class],
            ['msgpack',  Msgpack::class],
            ['none',     None::class],
            ['php',      Php::class],
        ];
    }

}

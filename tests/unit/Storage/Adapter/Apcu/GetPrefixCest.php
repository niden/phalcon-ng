<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Storage\Adapter\Apcu;

use PhalconNG\Storage\Adapter\Apcu;
use PhalconNG\Storage\SerializerFactory;
use PhalconNG\Test\Fixtures\Traits\ApcuTrait;
use UnitTester;

/**
 * Class GetPrefixCest
 */
class GetPrefixCest
{
    use ApcuTrait;

    /**
     * Tests PhalconNG\Storage\Adapter\Apcu :: getPrefix()
     *
     * @param UnitTester $I
     *
     * @author Phalcon Team <team@phalconphp.com>
     * @since  2019-03-31
     */
    public function storageAdapterApcuGetSetPrefix(UnitTester $I)
    {
        $I->wantToTest('Storage\Adapter\Apcu - getPrefix()');
        $serializer = new SerializerFactory();
        $adapter    = new Apcu($serializer, ['prefix' => 'my-prefix']);

        $expected = 'my-prefix';
        $actual   = $adapter->getPrefix();
        $I->assertEquals($expected, $actual);
    }

    /**
     * Tests PhalconNG\Storage\Adapter\Apcu :: getPrefix() - default
     *
     * @param UnitTester $I
     *
     * @author Phalcon Team <team@phalconphp.com>
     * @since  2019-03-31
     */
    public function storageAdapterApcuGetSetPrefixDefault(UnitTester $I)
    {
        $I->wantToTest('Storage\Adapter\Apcu - getPrefix() - default');
        $serializer = new SerializerFactory();
        $adapter    = new Apcu($serializer);

        $expected = 'ph-apcu-';
        $actual   = $adapter->getPrefix();
        $I->assertEquals($expected, $actual);
    }
}

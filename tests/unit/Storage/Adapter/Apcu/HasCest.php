<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Storage\Adapter\Apcu;

use PhalconNG\Storage\Adapter\Apcu;
use PhalconNG\Storage\SerializerFactory;
use PhalconNG\Test\Fixtures\Traits\ApcuTrait;
use UnitTester;

/**
 * Class HasCest
 */
class HasCest
{
    use ApcuTrait;

    /**
     * Tests PhalconNG\Storage\Adapter\Apcu :: get()
     *
     * @param UnitTester $I
     *
     * @author Phalcon Team <team@phalconphp.com>
     * @since  2019-03-31
     */
    public function storageAdapterApcuGetSetHas(UnitTester $I)
    {
        $I->wantToTest('Storage\Adapter\Apcu - has()');
        $serializer = new SerializerFactory();
        $adapter    = new Apcu($serializer);

        $key = uniqid();

        $actual = $adapter->has($key);
        $I->assertFalse($actual);

        $adapter->set($key, 'test');
        $actual = $adapter->has($key);
        $I->assertTrue($actual);
    }
}

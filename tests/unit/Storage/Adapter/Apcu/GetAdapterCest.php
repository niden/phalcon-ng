<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Storage\Adapter\Apcu;

use PhalconNG\Storage\Adapter\Apcu;
use PhalconNG\Storage\SerializerFactory;
use PhalconNG\Test\Fixtures\Traits\ApcuTrait;
use UnitTester;

/**
 * Class GetAdapterCest
 */
class GetAdapterCest
{
    use ApcuTrait;

    /**
     * Tests PhalconNG\Storage\Adapter\Apcu :: getAdapter()
     *
     * @param UnitTester $I
     *
     * @author Phalcon Team <team@phalconphp.com>
     * @since  2019-04-14
     */
    public function storageAdapterApcuGetAdapter(UnitTester $I)
    {
        $I->wantToTest('Storage\Adapter\Apcu - getAdapter()');
        $serializer = new SerializerFactory();
        $adapter    = new Apcu($serializer);

        $actual = $adapter->getAdapter();
        $I->assertNull($actual);
    }
}

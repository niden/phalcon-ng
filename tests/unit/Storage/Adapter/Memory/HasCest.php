<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Storage\Adapter\Memory;

use PhalconNG\Storage\Adapter\Memory;
use PhalconNG\Storage\SerializerFactory;
use UnitTester;

/**
 * Class HasCest
 */
class HasCest
{
    /**
     * Tests PhalconNG\Storage\Adapter\Memory :: get()
     *
     * @param UnitTester $I
     *
     * @author Phalcon Team <team@phalconphp.com>
     * @since  2019-03-31
     */
    public function storageAdapterMemoryGetSetHas(UnitTester $I)
    {
        $I->wantToTest('Storage\Adapter\Memory - has()');
        $serializer = new SerializerFactory();
        $adapter    = new Memory($serializer);

        $key = uniqid();

        $actual = $adapter->has($key);
        $I->assertFalse($actual);

        $adapter->set($key, 'test');
        $actual = $adapter->has($key);
        $I->assertTrue($actual);
    }
}

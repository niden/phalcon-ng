<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Storage\Adapter\Memory;

use PhalconNG\Storage\Adapter\Memory;
use PhalconNG\Storage\SerializerFactory;
use UnitTester;

/**
 * Class DeleteCest
 */
class DeleteCest
{
    /**
     * Tests PhalconNG\Storage\Adapter\Memory :: delete()
     *
     * @param UnitTester $I
     *
     * @author Phalcon Team <team@phalconphp.com>
     * @since  2019-03-31
     */
    public function storageAdapterMemoryDelete(UnitTester $I)
    {
        $I->wantToTest('Storage\Adapter\Memory - delete()');
        $serializer = new SerializerFactory();
        $adapter    = new Memory($serializer);

        $key = 'cache-data';
        $adapter->set($key, 'test');
        $actual = $adapter->has($key);
        $I->assertTrue($actual);

        $actual = $adapter->delete($key);
        $I->assertTrue($actual);

        $actual = $adapter->has($key);
        $I->assertFalse($actual);
    }

    /**
     * Tests PhalconNG\Storage\Adapter\Memory :: delete() - twice
     *
     * @param UnitTester $I
     *
     * @author Phalcon Team <team@phalconphp.com>
     * @since  2019-03-31
     */
    public function storageAdapterMemoryDeleteTwice(UnitTester $I)
    {
        $I->wantToTest('Storage\Adapter\Memory - delete() - twice');
        $serializer = new SerializerFactory();
        $adapter    = new Memory($serializer);

        $key = 'cache-data';
        $adapter->set($key, 'test');
        $actual = $adapter->has($key);
        $I->assertTrue($actual);

        $actual = $adapter->delete($key);
        $I->assertTrue($actual);

        $actual = $adapter->delete($key);
        $I->assertFalse($actual);
    }

    /**
     * Tests PhalconNG\Storage\Adapter\Memory :: delete() - unknown
     *
     * @param UnitTester $I
     *
     * @author Phalcon Team <team@phalconphp.com>
     * @since  2019-03-31
     */
    public function storageAdapterMemoryDeleteUnknown(UnitTester $I)
    {
        $I->wantToTest('Storage\Adapter\Memory - delete() - unknown');
        $serializer = new SerializerFactory();
        $adapter    = new Memory($serializer);

        $key    = 'cache-data';
        $actual = $adapter->delete($key);
        $I->assertFalse($actual);
    }
}

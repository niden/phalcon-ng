<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Storage\AdapterFactory;

use Codeception\Example;
use function getOptionsLibmemcached;
use function getOptionsRedis;
use function outputDir;
use PhalconNG\Storage\Adapter\Apcu;
use PhalconNG\Storage\Adapter\Libmemcached;
use PhalconNG\Storage\Adapter\Memory;
use PhalconNG\Storage\Adapter\Redis;
use PhalconNG\Storage\Adapter\Stream;
use PhalconNG\Storage\AdapterFactory;
use PhalconNG\Storage\SerializerFactory;
use PhalconNG\Storage\Exception;
use UnitTester;

/**
 * Class NewInstanceCest
 */
class NewInstanceCest
{
    /**
     * Tests PhalconNG\Storage\AdapterFactory :: newInstance()
     *
     * @dataProvider getExamples
     *
     * @param UnitTester $I
     *
     * @author Phalcon Team <team@phalconphp.com>
     * @since  2019-05-04
     *
     * @throws Exception
     */
    public function storageAdapterFactoryNewInstance(UnitTester $I, Example $example)
    {
        $I->wantToTest('Storage\AdapterFactory - newInstance() - ' . $example[0]);

        $serializer = new SerializerFactory();
        $adapter    = new AdapterFactory($serializer);

        $service = $adapter->newInstance($example[0], $example[2]);

        $class = $example[1];
        $I->assertInstanceOf($class, $service);
    }

    /**
     * Tests PhalconNG\Storage\SerializerFactory :: newInstance() - exception
     *
     * @param UnitTester $I
     *
     * @author Phalcon Team <team@phalconphp.com>
     * @since  2019-05-04
     *
     * @throws Exception
     */
    public function storageSerializerFactoryNewInstanceException(UnitTester $I)
    {
        $I->wantToTest('Storage\SerializerFactory - newInstance() - exception');

        $I->expectThrowable(
            new Exception('Service unknown is not registered'),
            function () {
                $serializer = new SerializerFactory();
                $adapter    = new AdapterFactory($serializer);

                $service = $adapter->newInstance('unknown');
            }
        );
    }

    /**
     * @return array
     */
    private function getExamples(): array
    {
        return [
            ['apcu',         Apcu::class,         []],
            ['libmemcached', Libmemcached::class, getOptionsLibmemcached()],
            ['memory',       Memory::class,       []],
            ['redis',        Redis::class,        getOptionsRedis()],
            ['stream',       Stream::class,       ['cacheDir' => outputDir()]],
        ];
    }
}

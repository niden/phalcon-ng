<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Storage;

use UnitTester;

/**
 * Class ConstructorCest
 */
class ConstructorCest
{
    /**
     * Tests PhalconNG\Storage\SerializerFactory :: _-construct()
     *
     * @param UnitTester $I
     *
     * @author Phalcon Team <team@phalconphp.com>
     * @since  2019-05-04
     */
    public function storageSerializerfactoryConstruct(UnitTester $I)
    {
        $I->wantToTest('Storage\SerializerFactory - __construct()');
    }
}

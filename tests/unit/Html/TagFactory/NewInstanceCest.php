<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Html\TagFactory;

use Codeception\Example;
use PhalconNG\Escaper;
use PhalconNG\Html\Helper\Anchor;
use PhalconNG\Html\Helper\AnchorRaw;
use PhalconNG\Html\Helper\Body;
use PhalconNG\Html\Helper\Button;
use PhalconNG\Html\Helper\Close;
use PhalconNG\Html\Helper\Element;
use PhalconNG\Html\Helper\ElementRaw;
use PhalconNG\Html\Helper\Form;
use PhalconNG\Html\Helper\Img;
use PhalconNG\Html\Helper\Label;
use PhalconNG\Html\Helper\TextArea;
use PhalconNG\Html\TagFactory;
use PhalconNG\Html\Exception;
use UnitTester;

/**
 * Class NewInstanceCest
 */
class NewInstanceCest
{
    /**
     * Tests PhalconNG\Filter\FilterFactory :: newInstance() - services
     *
     * @dataProvider getData
     *
     * @param UnitTester $I
     * @param Example    $example
     */
    public function filterFilterFactoryNewInstanceServices(UnitTester $I, Example $example)
    {
        $I->wantToTest('Filter\FilterLocatorFactory - newInstance() - services ' . $example[0]);
        $escaper = new Escaper();
        $factory = new TagFactory($escaper);
        $service = $factory->newInstance($example[0]);

        $class = $example[1];
        $I->assertInstanceOf($class, $service);
    }

    /**
     * Tests PhalconNG\Storage\SerializerFactory :: newInstance() - exception
     *
     * @param UnitTester $I
     *
     * @throws Exception
     * @since  2019-05-04
     *
     * @author Phalcon Team <team@phalconphp.com>
     */
    public function filterFilterFactoryNewInstanceException(UnitTester $I)
    {
        $I->wantToTest('Filter\FilterFactory - newInstance() - exception');

        $I->expectThrowable(
            new Exception('Service unknown is not registered'),
            function () {
                $escaper = new Escaper();
                $factory = new TagFactory($escaper);
                $service = $factory->newInstance('unknown');
            }
        );
    }

    /**
     * Returns the example data
     */
    private function getData(): array
    {
        return [
            ['a', Anchor::class],
            ['aRaw', AnchorRaw::class],
            ['body', Body::class],
            ['button', Button::class],
            ['close', Close::class],
            //            ['doctype'],
            ['element', Element::class],
            ['elementRaw', ElementRaw::class],
            ['form', Form::class],
            //            ['head'],
            ['img', Img::class],
            //            ['input'],
            ['label', Label::class],
            //            ['link'],
            //            ['script'],
            //            ['style'],
            ['textarea', TextArea::class],
            //            ['title'],
        ];
    }
}

<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Html\Helper\Element;

use Codeception\Example;
use PhalconNG\Escaper;
use PhalconNG\Html\Exception;
use PhalconNG\Html\Helper\ElementRaw;
use PhalconNG\Html\TagFactory;
use UnitTester;

/**
 * Class ElementRawCest
 */
class ElementRawCest
{
    /**
     * Tests PhalconNG\Html\Helper\Element :: __construct()
     *
     * @dataProvider getExamples
     *
     * @param UnitTester $I
     * @param Example    $example
     *
     * @throws Exception
     */
    public function htmlHelperElementRawConstruct(UnitTester $I, Example $example)
    {
        $I->wantToTest('Html\Helper\ElementRaw - __construct()');
        $escaper = new Escaper();
        $helper  = new ElementRaw($escaper);

        $expected = $example[0];
        $actual   = $helper($example[1], $example[2], $example[3]);
        $I->assertEquals($expected, $actual);

        $factory  = new TagFactory($escaper);
        $locator  = $factory->newInstance('elementRaw');
        $expected = $example[0];
        $actual   = $locator($example[1], $example[2], $example[3]);
        $I->assertEquals($expected, $actual);
    }

    /**
     * @return array
     */
    private function getExamples(): array
    {
        return [
            [
                '<canvas>test tag</canvas>',
                'canvas',
                'test tag',
                [],
            ],
            [
                '<canvas>Jack & Jill</canvas>',
                'canvas',
                'Jack & Jill',
                [],
            ],
            [
                '<canvas><script>alert("hello");</script>test tag</canvas>',
                'canvas',
                '<script>alert("hello");</script>test tag',
                [],
            ],
            [
                '<section id="my-id" name="my-name">test tag</section>',
                'section',
                'test tag',
                [
                    'id'   => 'my-id',
                    'name' => 'my-name',
                ],
            ],
            [
                '<address id="my-id" name="my-name" class="my-class">test tag</address>',
                'address',
                'test tag',
                [
                    'class' => 'my-class',
                    'name'  => 'my-name',
                    'id'    => 'my-id',
                ],
            ],
        ];
    }
}

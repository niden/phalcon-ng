<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Html\Helper;

use PhalconNG\Escaper;
use PhalconNG\Html\Exception;
use PhalconNG\Html\Helper\Close;
use PhalconNG\Html\TagFactory;
use UnitTester;

/**
 * Class CloseCest
 */
class CloseCest
{
    /**
     * Tests PhalconNG\Html\Helper\Close :: __construct()
     *
     * @param UnitTester $I
     *
     * @throws Exception
     */
    public function htmlHelperCloseConstruct(UnitTester $I)
    {
        $I->wantToTest('Html\Helper\FormClose - __construct()');
        $escaper = new Escaper();
        $helper  = new Close($escaper);

        $expected = '</form>';
        $actual   = $helper('form');
        $I->assertEquals($expected, $actual);

        $factory = new TagFactory($escaper);
        $locator = $factory->newInstance('close');
        $actual  = $locator('form');
        $I->assertEquals($expected, $actual);
    }
}

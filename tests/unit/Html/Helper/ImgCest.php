<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Html\Helper;

use Codeception\Example;
use PhalconNG\Escaper;
use PhalconNG\Html\Exception;
use PhalconNG\Html\Helper\Img;
use PhalconNG\Html\TagFactory;
use UnitTester;

/**
 * Class ImgCest
 */
class ImgCest
{
    /**
     * Tests PhalconNG\Html\Helper\Img :: __construct()
     *
     * @dataProvider getExamples
     *
     * @param UnitTester $I
     * @param Example    $example
     *
     * @throws Exception
     */
    public function htmlHelperImgConstruct(UnitTester $I, Example $example)
    {
        $I->wantToTest('Html\Helper\Img - __construct()');
        $escaper = new Escaper();
        $helper  = new Img($escaper);

        $expected = $example[0];
        $actual   = $helper('/my-url', $example[1]);
        $I->assertEquals($expected, $actual);

        $factory  = new TagFactory($escaper);
        $locator  = $factory->newInstance('img');
        $expected = $example[0];
        $actual   = $locator('/my-url', $example[1]);
        $I->assertEquals($expected, $actual);
    }

    /**
     * @return array
     */
    private function getExamples(): array
    {
        return [
            [
                '<img src="/my-url"/>',
                [],
            ],
            [
                '<img src="/my-url" id="my-id" name="my-name"/>',
                [
                    'id'   => 'my-id',
                    'name' => 'my-name',
                ],
            ],
            [
                '<img src="/my-url" id="my-id" name="my-name" class="my-class"/>',
                [
                    'src'   => '/other-url',
                    'class' => 'my-class',
                    'name'  => 'my-name',
                    'id'    => 'my-id',
                ],
            ],
        ];
    }
}

<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Html\Breadcrumbs;

use PhalconNG\Html\Breadcrumbs;
use UnitTester;

/**
 * Class GetSetSeparatorCest
 */
class GetSetSeparatorCest
{
    /**
     * Tests PhalconNG\Html\Breadcrumbs :: getSeparator()/setSeparator()
     *
     * @param UnitTester $I
     */
    public function htmlBreadcrumbsGetSetSeparator(UnitTester $I)
    {
        $I->wantToTest('Html\Breadcrumbs - getSeparator()/setSeparator()');
        $breadcrumbs = new Breadcrumbs();

        $expected = ' -:- ';
        $breadcrumbs->setSeparator($expected);
        $actual = $breadcrumbs->getSeparator();
        $I->assertEquals($expected, $actual);
    }
}

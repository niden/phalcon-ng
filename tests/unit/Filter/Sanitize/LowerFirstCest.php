<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Filter\Sanitize;

use Codeception\Example;
use PhalconNG\Filter\Sanitize\LowerFirst;
use UnitTester;

/**
 * Class LowerFirstCest
 */
class LowerFirstCest
{
    /**
     * Tests PhalconNG\Filter\Sanitize\LowerFirst :: __invoke()
     *
     * @dataProvider getData
     *
     * @param UnitTester $I
     * @param Example    $example
     */
    public function filterSanitizeLowerFirstInvoke(UnitTester $I, Example $example)
    {
        $I->wantToTest('Filter\Sanitize\LowerFirst - __invoke()');

        $sanitizer = new LowerFirst();

        $actual = $sanitizer($example[0]);
        $I->assertEquals($example[1], $actual);
    }

    /**
     * @return array
     */
    private function getData(): array
    {
        return [
            ['test', 'test'],
            ['tEsT', 'tEsT'],
            ['TEST', 'tEST'],
        ];
    }
}

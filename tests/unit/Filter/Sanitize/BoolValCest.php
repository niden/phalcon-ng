<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Filter\Sanitize;

use Codeception\Example;
use PhalconNG\Filter\Sanitize\BoolVal;
use UnitTester;

/**
 * Class BoolValCest
 */
class BoolValCest
{
    /**
     * Tests PhalconNG\Filter\Sanitize\BoolVal :: __invoke()
     *
     * @dataProvider getData
     *
     * @param UnitTester $I
     * @param Example    $example
     */
    public function filterSanitizeBoolValInvoke(UnitTester $I, Example $example)
    {
        $I->wantToTest('Filter\Sanitize\BoolVal - __invoke()');

        $sanitizer = new BoolVal();

        $actual = $sanitizer($example[0]);
        $I->assertEquals($example[1], $actual);
    }

    /**
     * @return array
     */
    private function getData(): array
    {
        return [
            [1000, true],
            [0xFFA, true],
            ['1000', true],
            [null, false],
            ['on', true],
            ['off', false],
        ];
    }
}

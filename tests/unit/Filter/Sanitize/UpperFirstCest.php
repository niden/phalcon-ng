<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Filter\Sanitize;

use Codeception\Example;
use PhalconNG\Filter\Sanitize\UpperFirst;
use UnitTester;

/**
 * Class UpperFirstCest
 */
class UpperFirstCest
{
    /**
     * Tests PhalconNG\Filter\Sanitize\UpperFirst :: __invoke()
     *
     * @dataProvider getData
     *
     * @param UnitTester $I
     * @param Example    $example
     */
    public function filterSanitizeUpperFirstInvoke(UnitTester $I, Example $example)
    {
        $I->wantToTest('Filter\Sanitize\UpperFirst - __invoke()');

        $sanitizer = new UpperFirst();

        $actual = $sanitizer($example[0]);
        $I->assertEquals($example[1], $actual);
    }

    /**
     * @return array
     */
    private function getData(): array
    {
        return [
            ['test', 'Test'],
            ['tEsT', 'TEsT'],
            ['TEST', 'TEST'],
        ];
    }
}

<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Filter\FilterFactory;

use Codeception\Example;
use PhalconNG\Filter\FilterFactory;
use PhalconNG\Filter\Sanitize\AbsInt;
use PhalconNG\Filter\Sanitize\Alnum;
use PhalconNG\Filter\Sanitize\Alpha;
use PhalconNG\Filter\Sanitize\BoolVal;
use PhalconNG\Filter\Sanitize\Email;
use PhalconNG\Filter\Sanitize\FloatVal;
use PhalconNG\Filter\Sanitize\IntVal;
use PhalconNG\Filter\Sanitize\Lower;
use PhalconNG\Filter\Sanitize\LowerFirst;
use PhalconNG\Filter\Sanitize\Regex;
use PhalconNG\Filter\Sanitize\Remove;
use PhalconNG\Filter\Sanitize\Replace;
use PhalconNG\Filter\Sanitize\Special;
use PhalconNG\Filter\Sanitize\SpecialFull;
use PhalconNG\Filter\Sanitize\StringVal;
use PhalconNG\Filter\Sanitize\Striptags;
use PhalconNG\Filter\Sanitize\Trim;
use PhalconNG\Filter\Sanitize\Upper;
use PhalconNG\Filter\Sanitize\UpperFirst;
use PhalconNG\Filter\Sanitize\UpperWords;
use PhalconNG\Filter\Sanitize\Url;
use PhalconNG\Filter\Exception;
use UnitTester;

/**
 * Class NewInstanceCest
 */
class NewInstanceCest
{
    /**
     * Tests PhalconNG\Filter\FilterFactory :: newInstance() - exception
     *
     * @param UnitTester $I
     *
     * @throws Exception
     */
    public function filterFilterFactoryNewInstanceException(UnitTester $I)
    {
        $I->wantToTest('Filter\FilterFactory - newInstance() - exception');
        $I->expectThrowable(
            new Exception('Service unknown is not registered'),
            function () {
                $factory  = new FilterFactory();
                $instance = $factory->newInstance('unknown');
            }
        );
    }

    /**
     * Tests PhalconNG\Filter\FilterFactory :: newInstance() - services
     *
     * @dataProvider getData
     *
     * @param UnitTester $I
     * @param Example    $example
     *
     * @throws Exception
     */
    public function filterFilterFactoryNewInstanceServices(UnitTester $I, Example $example)
    {
        $I->wantToTest('Filter\FilterFactory - newInstance() - services ' . $example[0]);
        $factory  = new FilterFactory();
        $instance = $factory->newInstance($example[0]);

        $I->assertInstanceOf($example[1], $instance);
    }

    /**
     * Returns the example data
     */
    private function getData(): array
    {
        return [
            [FilterFactory::FILTER_ABSINT,     AbsInt::class],
            [FilterFactory::FILTER_ALNUM,      Alnum::class],
            [FilterFactory::FILTER_ALPHA,      Alpha::class],
            [FilterFactory::FILTER_BOOL,       BoolVal::class],
            [FilterFactory::FILTER_EMAIL,      Email::class],
            [FilterFactory::FILTER_FLOAT,      FloatVal::class],
            [FilterFactory::FILTER_INT,        IntVal::class],
            [FilterFactory::FILTER_LOWER,      Lower::class],
            [FilterFactory::FILTER_LOWERFIRST, LowerFirst::class],
            [FilterFactory::FILTER_REGEX,      Regex::class],
            [FilterFactory::FILTER_REMOVE,     Remove::class],
            [FilterFactory::FILTER_REPLACE,    Replace::class],
            [FilterFactory::FILTER_SPECIAL,    Special::class],
            [FilterFactory::FILTER_SPECIALFULL,SpecialFull::class],
            [FilterFactory::FILTER_STRING,     StringVal::class],
            [FilterFactory::FILTER_STRIPTAGS,  Striptags::class],
            [FilterFactory::FILTER_TRIM,       Trim::class],
            [FilterFactory::FILTER_UPPER,      Upper::class],
            [FilterFactory::FILTER_UPPERFIRST, UpperFirst::class],
            [FilterFactory::FILTER_UPPERWORDS, UpperWords::class],
            [FilterFactory::FILTER_URL,        Url::class],
        ];
    }
}

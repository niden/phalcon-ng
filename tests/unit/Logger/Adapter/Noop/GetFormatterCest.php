<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Logger\Adapter\Noop;

use PhalconNG\Logger\Adapter\Noop;
use PhalconNG\Logger\Formatter\FormatterInterface;
use PhalconNG\Logger\Formatter\Line;
use UnitTester;

/**
 * Class GetFormatterCest
 *
 * @package PhalconNG\Test\Unit\Logger
 */
class GetFormatterCest
{
    /**
     * Tests PhalconNG\Logger\Adapter\Noop :: getFormatter()
     *
     * @param UnitTester $I
     */
    public function loggerAdapterNoopGetFormatter(UnitTester $I)
    {
        $I->wantToTest('Logger\Adapter\Noop - getFormatter()');

        $adapter = new Noop();
        $adapter->getFormatter(new Line());

        $class  = FormatterInterface::class;
        $actual = $adapter->getFormatter();
        $I->assertInstanceOf($class, $actual);
    }
}

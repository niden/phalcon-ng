<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Logger\Adapter\Noop;

use PhalconNG\Logger\Adapter\Noop;
use UnitTester;

/**
 * Class CloseCest
 *
 * @package PhalconNG\Test\Unit\Logger
 */
class CloseCest
{
    /**
     * Tests PhalconNG\Logger\Adapter\Noop :: close()
     *
     * @param UnitTester $I
     */
    public function loggerAdapterNoopClose(UnitTester $I)
    {
        $I->wantToTest('Logger\Adapter\Noop - close()');
        $adapter = new Noop();

        $actual = $adapter->close();
        $I->assertTrue($actual);
    }
}

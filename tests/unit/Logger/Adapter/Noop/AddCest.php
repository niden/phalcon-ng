<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Logger\Adapter\Noop;

use PhalconNG\Logger\Adapter\Noop;
use PhalconNG\Logger\Item;
use PhalconNG\Logger\Logger;
use UnitTester;

/**
 * Class AddCest
 *
 * @package PhalconNG\Test\Unit\Logger
 */
class AddCest
{
    /**
     * Tests PhalconNG\Logger\Adapter\Noop :: add()
     *
     * @param UnitTester $I
     */
    public function loggerAdapterNoopAdd(UnitTester $I)
    {
        $I->wantToTest('Logger\Adapter\Noop - add()');
        $adapter = new Noop();

        $adapter->begin();
        $item1 = new Item('Message 1', 'debug', Logger::DEBUG);
        $item2 = new Item('Message 2', 'debug', Logger::DEBUG);
        $item3 = new Item('Message 3', 'debug', Logger::DEBUG);

        $adapter
            ->add($item1)
            ->add($item2)
            ->add($item3)
        ;

        $adapter->commit();
    }
}

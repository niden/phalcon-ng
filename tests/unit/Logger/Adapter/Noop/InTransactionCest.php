<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Logger\Adapter\Noop;

use PhalconNG\Logger\Adapter\Noop;
use UnitTester;

/**
 * Class InTransactionCest
 *
 * @package PhalconNG\Test\Unit\Logger
 */
class InTransactionCest
{
    /**
     * Tests PhalconNG\Logger\Adapter\Noop :: inTransaction()
     *
     * @param UnitTester $I
     */
    public function loggerAdapterNoopInTransaction(UnitTester $I)
    {
        $I->wantToTest('Logger\Adapter\Noop - inTransaction()');
        $adapter = new Noop();

        $adapter->begin();

        $actual = $adapter->inTransaction();
        $I->assertTrue($actual);

        $adapter->commit();

        $actual = $adapter->inTransaction();
        $I->assertFalse($actual);
    }
}

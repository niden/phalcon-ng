<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Logger\Adapter\Noop;

use PhalconNG\Logger\Adapter\Noop;
use PhalconNG\Logger\Item;
use PhalconNG\Logger\Logger;
use UnitTester;

/**
 * Class ProcessCest
 *
 * @package PhalconNG\Test\Unit\Logger
 */
class ProcessCest
{
    /**
     * Tests PhalconNG\Logger\Adapter\Noop :: process()
     *
     * @param UnitTester $I
     */
    public function loggerAdapterNoopProcess(UnitTester $I)
    {
        $I->wantToTest('Logger\Adapter\Noop - process()');
        $adapter = new Noop();

        $item = new Item('Message 1', 'debug', Logger::DEBUG);
        $adapter->process($item);
    }
}

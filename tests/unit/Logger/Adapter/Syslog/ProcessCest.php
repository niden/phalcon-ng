<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Logger\Adapter\Syslog;

use PhalconNG\Logger\Adapter\Syslog;
use PhalconNG\Logger\Exception;
use PhalconNG\Logger\Formatter\Line;
use PhalconNG\Logger\Item;
use PhalconNG\Logger\Logger;
use UnitTester;

/**
 * Class ProcessCest
 *
 * @package PhalconNG\Test\Unit\Logger
 */
class ProcessCest
{
    /**
     * Tests PhalconNG\Logger\Adapter\Syslog :: process()
     *
     * @param UnitTester $I
     *
     * @throws Exception
     */
    public function loggerAdapterSyslogProcess(UnitTester $I)
    {
        $I->wantToTest('Logger\Adapter\Syslog - process()');
        $streamName = $I->getNewFileName('log', 'log');
        $adapter    = new Syslog($streamName);

        $item = new Item('Message 1', 'debug', Logger::DEBUG);
        $adapter->process($item);

        $actual = $adapter->close();
        $I->assertTrue($actual);
    }

    /**
     * Tests PhalconNG\Logger\Adapter\Syslog :: process() - exception
     *
     * @param UnitTester $I
     *
     * @throws Exception
     */
    public function loggerAdapterSyslogProcessException(UnitTester $I)
    {
        $I->wantToTest('Logger\Adapter\Syslog - process() - exception');
        $I->expectThrowable(
            new Exception('The formatted message is not valid'),
            function () use ($I) {
                $streamName = $I->getNewFileName('log', 'log');
                $formatter  = new Line();
                $adapter    = new Syslog($streamName);
                $adapter->setFormatter($formatter);

                $item = new Item('Message 1', 'debug', Logger::DEBUG);
                $adapter->process($item);
            }
        );
    }
}

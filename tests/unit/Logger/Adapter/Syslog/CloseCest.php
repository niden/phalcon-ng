<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Logger\Adapter\Syslog;

use PhalconNG\Logger\Adapter\Syslog;
use UnitTester;

/**
 * Class CloseCest
 *
 * @package PhalconNG\Test\Unit\Logger
 */
class CloseCest
{
    /**
     * Tests PhalconNG\Logger\Adapter\Syslog :: close()
     *
     * @param UnitTester $I
     */
    public function loggerAdapterSyslogClose(UnitTester $I)
    {
        $I->wantToTest('Logger\Adapter\Syslog - close()');
        $streamName = $I->getNewFileName('log', 'log');
        $adapter    = new Syslog($streamName);

        $actual = $adapter->close();
        $I->assertTrue($actual);
    }
}

<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Logger\Adapter\Syslog;

use PhalconNG\Logger\Adapter\Syslog;
use UnitTester;

/**
 * Class RollbackCest
 *
 * @package PhalconNG\Test\Unit\Logger
 */
class RollbackCest
{
    /**
     * Tests PhalconNG\Logger\Adapter\Syslog :: rollback()
     *
     * @param UnitTester $I
     */
    public function loggerAdapterSyslogRollback(UnitTester $I)
    {
        $I->wantToTest('Logger\Adapter\Syslog - rollback()');
        $streamName = $I->getNewFileName('log', 'log');
        $adapter    = new Syslog($streamName);

        $adapter->begin();

        $actual = $adapter->inTransaction();
        $I->assertTrue($actual);

        $adapter->rollback();

        $actual = $adapter->inTransaction();
        $I->assertFalse($actual);
    }
}

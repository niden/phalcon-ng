<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Logger\Adapter\Syslog;

use PhalconNG\Logger\Adapter\Syslog;
use PhalconNG\Logger\Exception;
use UnitTester;

/**
 * Class CommitCest
 *
 * @package PhalconNG\Test\Unit\Logger
 */
class CommitCest
{
    /**
     * Tests PhalconNG\Logger\Adapter\Syslog :: commit()
     *
     * @param UnitTester $I
     */
    public function loggerAdapterSyslogCommit(UnitTester $I)
    {
        $I->wantToTest('Logger\Adapter\Syslog - commit()');
        $streamName = $I->getNewFileName('log', 'log');
        $adapter    = new Syslog($streamName);

        $adapter->begin();

        $actual = $adapter->inTransaction();
        $I->assertTrue($actual);

        $adapter->commit();

        $actual = $adapter->inTransaction();
        $I->assertFalse($actual);
    }

    /**
     * Tests PhalconNG\Logger\Adapter\Syslog :: commit() - no transaction
     *
     * @param UnitTester $I
     */
    public function loggerAdapterSyslogCommitNoTransaction(UnitTester $I)
    {
        $I->wantToTest('Logger\Adapter\Syslog - commit() - no transaction');
        $streamName = $I->getNewFileName('log', 'log');

        try {
            $adapter = new Syslog($streamName);

            $actual = $adapter->inTransaction();
            $I->assertFalse($actual);

            $adapter->commit();
        } catch (Exception $ex) {
            $expected = 'There is no active transaction';
            $actual   = $ex->getMessage();
            $I->assertEquals($expected, $actual);
        }
    }
}

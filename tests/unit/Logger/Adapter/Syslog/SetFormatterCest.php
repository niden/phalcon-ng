<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Logger\Adapter\Syslog;

use PhalconNG\Logger\Adapter\Syslog;
use PhalconNG\Logger\Formatter\FormatterInterface;
use PhalconNG\Logger\Formatter\Line;
use UnitTester;

/**
 * Class SetFormatterCest
 *
 * @package PhalconNG\Test\Unit\Logger
 */
class SetFormatterCest
{
    /**
     * Tests PhalconNG\Logger\Adapter\Syslog :: setFormatter()
     *
     * @param UnitTester $I
     */
    public function loggerAdapterSyslogSetFormatter(UnitTester $I)
    {
        $I->wantToTest('Logger\Adapter\Syslog - setFormatter()');

        $streamName = $I->getNewFileName('log', 'log');

        $adapter = new Syslog($streamName);
        $adapter->setFormatter(new Line());

        $class  = FormatterInterface::class;
        $actual = $adapter->getFormatter();
        $I->assertInstanceOf($class, $actual);
    }
}

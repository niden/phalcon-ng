<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Logger\Adapter\Stream;

use PhalconNG\Logger\Adapter\Stream;
use PhalconNG\Logger\Exception;
use UnitTester;

/**
 * Class CommitCest
 *
 * @package PhalconNG\Test\Unit\Logger
 */
class CommitCest
{
    /**
     * Tests PhalconNG\Logger\Adapter\Stream :: commit()
     *
     * @param UnitTester $I
     */
    public function loggerAdapterStreamCommit(UnitTester $I)
    {
        $I->wantToTest('Logger\Adapter\Stream - commit()');
        $fileName   = $I->getNewFileName('log', 'log');
        $outputPath = outputDir();
        $adapter    = new Stream($outputPath . $fileName);

        $adapter->begin();

        $actual = $adapter->inTransaction();
        $I->assertTrue($actual);

        $adapter->commit();

        $actual = $adapter->inTransaction();
        $I->assertFalse($actual);

        $I->safeDeleteFile($outputPath . $fileName);
    }

    /**
     * Tests PhalconNG\Logger\Adapter\Stream :: commit() - no transaction
     *
     * @param UnitTester $I
     */
    public function loggerAdapterStreamCommitNoTransaction(UnitTester $I)
    {
        $I->wantToTest('Logger\Adapter\Stream - commit() - no transaction');
        $fileName   = $I->getNewFileName('log', 'log');
        $outputPath = outputDir();

        try {
            $adapter = new Stream($outputPath . $fileName);

            $actual = $adapter->inTransaction();
            $I->assertFalse($actual);

            $adapter->commit();
        } catch (Exception $ex) {
            $expected = 'There is no active transaction';
            $actual   = $ex->getMessage();
            $I->assertEquals($expected, $actual);
        }

        $I->safeDeleteFile($outputPath . $fileName);
    }
}

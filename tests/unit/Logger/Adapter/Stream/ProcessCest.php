<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Logger\Adapter\Stream;

use PhalconNG\Logger\Adapter\Stream;
use PhalconNG\Logger\Exception;
use PhalconNG\Logger\Logger;
use PhalconNG\Logger\Item;
use UnitTester;
use function outputDir;

/**
 * Class ProcessCest
 *
 * @package PhalconNG\Test\Unit\Logger
 */
class ProcessCest
{
    /**
     * Tests PhalconNG\Logger\Adapter\Stream :: process()
     *
     * @param UnitTester $I
     *
     * @throws Exception
     */
    public function loggerAdapterStreamProcess(UnitTester $I)
    {
        $I->wantToTest('Logger\Adapter\Stream - process()');
        $fileName   = $I->getNewFileName('log', 'log');
        $outputPath = outputDir();
        $adapter    = new Stream($outputPath . $fileName);

        $item = new Item('Message 1', 'debug', Logger::DEBUG);
        $adapter->process($item);

        $I->amInPath($outputPath);
        $I->seeFileFound($fileName);
        $I->openFile($fileName);
        $I->seeInThisFile('Message 1');

        $I->safeDeleteFile($outputPath . $fileName);
    }
}

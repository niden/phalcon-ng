<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Logger\Adapter\Stream;

use PhalconNG\Logger\Adapter\Stream;
use PhalconNG\Logger\Exception;
use UnitTester;
use function outputDir;

/**
 * Class RollbackCest
 *
 * @package PhalconNG\Test\Unit\Logger
 */
class RollbackCest
{
    /**
     * Tests PhalconNG\Logger\Adapter\Stream :: rollback()
     *
     * @param UnitTester $I
     *
     * @throws Exception
     */
    public function loggerAdapterStreamRollback(UnitTester $I)
    {
        $I->wantToTest('Logger\Adapter\Stream - rollback()');
        $fileName   = $I->getNewFileName('log', 'log');
        $outputPath = outputDir();
        $adapter    = new Stream($outputPath . $fileName);

        $adapter->begin();

        $actual = $adapter->inTransaction();
        $I->assertTrue($actual);

        $adapter->rollback();

        $actual = $adapter->inTransaction();
        $I->assertFalse($actual);

        $I->safeDeleteFile($outputPath . $fileName);
    }

    /**
     * Tests PhalconNG\Logger\Adapter\Stream :: rollback() - exception
     *
     * @param UnitTester $I
     */
    public function loggerAdapterStreamRollbackException(UnitTester $I)
    {
        $I->wantToTest('Logger\Adapter\Stream - rollback() - exception');
        $I->expectThrowable(
            new Exception('There is no active transaction'),
            function () use ($I) {
                $fileName   = $I->getNewFileName('log', 'log');
                $outputPath = outputDir();
                $adapter    = new Stream($outputPath . $fileName);

                $adapter->rollback();
            }
        );
    }
}

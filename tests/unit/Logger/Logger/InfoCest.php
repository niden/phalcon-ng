<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Logger\Logger;

use PhalconNG\Test\Fixtures\Traits\LoggerTrait;
use UnitTester;

/**
 * Class InfoCest
 *
 * @package PhalconNG\Test\Unit\Logger
 */
class InfoCest
{
    use LoggerTrait;

    /**
     * Tests PhalconNG\Logger :: info()
     *
     * @param UnitTester $I
     */
    public function loggerInfo(UnitTester $I)
    {
        $I->wantToTest('Logger - info()');
        $this->runLoggerFile($I, 'info');
    }
}

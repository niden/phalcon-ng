<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Logger\Logger;

use PhalconNG\Logger\Adapter\Stream;
use PhalconNG\Logger\Exception;
use PhalconNG\Logger\Logger;
use UnitTester;

/**
 * Class GetAdapterCest
 *
 * @package PhalconNG\Test\Unit\Logger
 */
class GetAdapterCest
{
    /**
     * Tests PhalconNG\Logger :: getAdapter()
     *
     * @param UnitTester $I
     */
    public function loggerGetAdapter(UnitTester $I)
    {
        $I->wantToTest('Logger - getAdapter()');
        $fileName1  = $I->getNewFileName('log', 'log');
        $outputPath = outputDir();
        $adapter1   = new Stream($outputPath . $fileName1);

        $logger = new Logger(
            'my-logger',
            [
                'one' => $adapter1,
            ]
        );


        $class  = Stream::class;
        $actual = $logger->getAdapter('one');
        $I->assertInstanceOf($class, $actual);

        $I->safeDeleteFile($outputPath . $fileName1);
    }

    /**
     * Tests PhalconNG\Logger :: getAdapter() - unknown
     *
     * @param UnitTester $I
     */
    public function loggerGetAdapterUnknown(UnitTester $I)
    {
        $I->wantToTest('Logger - getAdapter() - unknown');

        $I->expectThrowable(
            new Exception('Adapter does not exist for this logger'),
            function () {
                $logger = new Logger('my-logger');
                $logger->getAdapter('unknown');
            }
        );
    }
}

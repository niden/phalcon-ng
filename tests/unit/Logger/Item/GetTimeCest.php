<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Logger\Item;

use PhalconNG\Logger\Item;
use PhalconNG\Logger\Logger;
use UnitTester;

/**
 * Class GetTimeCest
 *
 * @package PhalconNG\Test\Unit\Logger
 */
class GetTimeCest
{
    /**
     * Tests PhalconNG\Logger\Item :: getTime()
     *
     * @param UnitTester $I
     */
    public function loggerItemGetTime(UnitTester $I)
    {
        $I->wantToTest('Logger\Item - getTime()');
        $time = time();
        $item = new Item('log message', 'debug', Logger::DEBUG, $time);

        $expected = $time;
        $actual   = $item->getTime();
        $I->assertEquals($expected, $actual);
    }
}

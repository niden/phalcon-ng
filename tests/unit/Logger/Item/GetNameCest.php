<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Logger\Item;

use PhalconNG\Logger\Item;
use PhalconNG\Logger\Logger;
use UnitTester;

/**
 * Class GetNameCest
 *
 * @package PhalconNG\Test\Unit\Logger
 */
class GetNameCest
{
    /**
     * Tests PhalconNG\Logger\Item :: getName()
     *
     * @param UnitTester $I
     */
    public function loggerItemGetName(UnitTester $I)
    {
        $I->wantToTest('Logger\Item - getName()');
        $time = time();
        $item = new Item('log message', 'debug', Logger::DEBUG, $time);

        $expected = 'debug';
        $actual   = $item->getName();
        $I->assertEquals($expected, $actual);
    }
}

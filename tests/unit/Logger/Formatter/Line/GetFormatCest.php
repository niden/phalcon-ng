<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Logger\Formatter\Line;

use PhalconNG\Logger\Formatter\Line;
use UnitTester;

/**
 * Class GetFormatCest
 *
 * @package PhalconNG\Test\Unit\Logger
 */
class GetFormatCest
{
    /**
     * Tests PhalconNG\Logger\Formatter\Line :: getFormat()
     *
     * @param UnitTester $I
     */
    public function loggerFormatterLineGetFormat(UnitTester $I)
    {
        $I->wantToTest('Logger\Formatter\Line - getFormat()');
        $formatter = new Line();

        $expected = '[%date%][%type%] %message%';
        $actual   = $formatter->getFormat();
        $I->assertEquals($expected, $actual);
    }
}

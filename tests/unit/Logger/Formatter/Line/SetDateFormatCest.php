<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Logger\Formatter\Line;

use PhalconNG\Logger\Formatter\Line;
use UnitTester;

/**
 * Class SetDateFormatCest
 *
 * @package PhalconNG\Test\Unit\Logger
 */
class SetDateFormatCest
{
    /**
     * Tests PhalconNG\Logger\Formatter\Line :: setDateFormat()
     *
     * @param UnitTester $I
     */
    public function loggerFormatterLineSetDateFormat(UnitTester $I)
    {
        $I->wantToTest('Logger\Formatter\Line - setDateFormat()');
        $formatter = new Line();

        $format = 'YmdHis';
        $formatter->setDateFormat($format);

        $expected = $format;
        $actual   = $formatter->getDateFormat();
        $I->assertEquals($expected, $actual);
    }
}

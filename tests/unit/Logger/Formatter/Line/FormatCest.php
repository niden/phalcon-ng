<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Logger\Formatter\Line;

use PhalconNG\Logger\Formatter\Line;
use PhalconNG\Logger\Item;
use PhalconNG\Logger\Logger;
use UnitTester;
use const PHP_EOL;

/**
 * Class FormatCest
 *
 * @package PhalconNG\Test\Unit\Logger
 */
class FormatCest
{
    /**
     * Tests PhalconNG\Logger\Formatter\Line :: format()
     *
     * @param UnitTester $I
     */
    public function loggerFormatterLineFormat(UnitTester $I)
    {
        $I->wantToTest('Logger\Formatter\Line - format()');
        $formatter = new Line();

        $time = time();
        $item = new Item('log message', 'debug', Logger::DEBUG, $time);

        $expected = sprintf('[%s][debug] log message', date('D, d M y H:i:s O', $time)) . PHP_EOL;
        $actual   = $formatter->format($item);
        $I->assertEquals($expected, $actual);
    }

    /**
     * Tests PhalconNG\Logger\Formatter\Line :: format() -custom
     *
     * @param UnitTester $I
     */
    public function loggerFormatterLineFormatCustom(UnitTester $I)
    {
        $I->wantToTest('Logger\Formatter\Line - format() - custom');
        $formatter = new Line('%message%-[%type%]-%date%');

        $time = time();
        $item = new Item('log message', 'debug', Logger::DEBUG, $time);

        $expected = sprintf('log message-[debug]-%s', date('D, d M y H:i:s O', $time)) . PHP_EOL;
        $actual   = $formatter->format($item);
        $I->assertEquals($expected, $actual);
    }
}

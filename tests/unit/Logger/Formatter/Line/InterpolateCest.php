<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Logger\Formatter\Line;

use PhalconNG\Logger\Formatter\Line;
use PhalconNG\Logger\Item;
use PhalconNG\Logger\Logger;
use UnitTester;

/**
 * Class InterpolateCest
 *
 * @package PhalconNG\Test\Unit\Logger
 */
class InterpolateCest
{
    /**
     * Tests PhalconNG\Logger\Formatter\Line :: interpolate()
     *
     * @param UnitTester $I
     */
    public function loggerFormatterLineInterpolate(UnitTester $I)
    {
        $I->wantToTest('Logger\Formatter\Line - interpolate()');
        $formatter = new Line();

        $message = 'The sky is {color}';
        $context = [
            'color' => 'blue',
        ];

        $expected = 'The sky is blue';
        $actual   = $formatter->interpolate($message, $context);
        $I->assertEquals($expected, $actual);
    }

    /**
     * Tests PhalconNG\Logger\Formatter\Line :: interpolate() - format
     *
     * @param UnitTester $I
     */
    public function loggerFormatterLineInterpolateFormat(UnitTester $I)
    {
        $I->wantToTest('Logger\Formatter\Line - interpolate() - format');
        $formatter = new Line();

        $message = 'The sky is {color}';
        $context = [
            'color' => 'blue',
        ];

        $time = time();
        $item = new Item($message, 'debug', Logger::DEBUG, $time, $context);

        $expected = sprintf('[%s][debug] The sky is blue', date('D, d M y H:i:s O', $time)) . PHP_EOL;
        $actual   = $formatter->format($item);
        $I->assertEquals($expected, $actual);
    }

    /**
     * Tests PhalconNG\Logger\Formatter\Line :: interpolate() - empty
     *
     * @param UnitTester $I
     */
    public function loggerFormatterLineInterpolateEmpty(UnitTester $I)
    {
        $I->wantToTest('Logger\Formatter\Line - interpolate() - empty');
        $formatter = new Line();

        $message = 'The sky is {color}';
        $context = [];

        $expected = 'The sky is {color}';
        $actual   = $formatter->interpolate($message, $context);
        $I->assertEquals($expected, $actual);
    }
}

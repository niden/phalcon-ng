<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Helper\Arr;

use PhalconNG\Helper\Arr;
use UnitTester;

/**
 * Class ChunkCest
 */
class ChunkCest
{
    /**
     * Tests PhalconNG\Helper\Arr :: chunk()
     *
     * @param UnitTester $I
     */
    public function helperArrChunk(UnitTester $I)
    {
        $I->wantToTest('Helper\Arr - chunk()');

        $source = [
            'k1' => 1,
            'k2' => 2,
            'k3' => 3,
            'k4' => 4,
            'k5' => 5,
            'k6' => 6,
        ];

        $expected = [
            [1, 2],
            [3, 4],
            [5, 6],
        ];
        $actual   = Arr::chunk($source, 2);
        $I->assertEquals($expected, $actual);
    }

    /**
     * Tests PhalconNG\Helper\Arr :: chunk() - preserve
     *
     * @param UnitTester $I
     */
    public function helperArrChunkPreserve(UnitTester $I)
    {
        $I->wantToTest('Helper\Arr - chunk() - preserve');

        $source = [
            'k1' => 1,
            'k2' => 2,
            'k3' => 3,
            'k4' => 4,
            'k5' => 5,
            'k6' => 6,
        ];

        $expected = [
            [
                'k1' => 1,
                'k2' => 2,
            ],
            [
                'k3' => 3,
                'k4' => 4,
            ],
            [
                'k5' => 5,
                'k6' => 6,
            ],
        ];
        $actual   = Arr::chunk($source, 2, true);
        $I->assertEquals($expected, $actual);
    }
}

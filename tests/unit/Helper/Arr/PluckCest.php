<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Helper\Arr;

use PhalconNG\Helper\Arr;
use stdClass;
use UnitTester;

/**
 * Class PluckCest
 */
class PluckCest
{
    /**
     * Tests PhalconNG\Helper\Arr :: pluck()
     *
     * @param UnitTester $I
     */
    public function helperArrPluck(UnitTester $I)
    {
        $I->wantToTest('Helper\Arr - pluck()');
        $collection = [
            ['productId' => 'prod-100', 'name' => 'Desk'],
            ['productId' => 'prod-200', 'name' => 'Chair'],
        ];

        $expected = ['Desk', 'Chair'];
        $actual   = Arr::pluck($collection, 'name');
        $I->assertEquals($expected, $actual);
    }

    /**
     * Tests PhalconNG\Helper\Arr :: pluck() - object
     *
     * @param UnitTester $I
     */
    public function helperArrPluckObject(UnitTester $I)
    {
        $I->wantToTest('Helper\Arr - pluck() - object');
        $obj1            = new stdClass();
        $obj1->productId = 'prod-100';
        $obj1->name      = 'Desk';

        $obj2            = new stdClass();
        $obj2->productId = 'prod-200';
        $obj2->name      = 'Chair';

        $collection = [$obj1, $obj2];
        $expected   = ['Desk', 'Chair'];
        $actual     = Arr::pluck($collection, 'name');
        $I->assertEquals($expected, $actual);
    }
}

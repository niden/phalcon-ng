<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Helper\Arr;

use PhalconNG\Helper\Arr;
use stdClass;
use UnitTester;

/**
 * Class GroupCest
 */
class GroupCest
{
    /**
     * Tests PhalconNG\Helper\Arr :: group()
     *
     * @param UnitTester $I
     */
    public function helperArrGroup(UnitTester $I)
    {
        $I->wantToTest('Helper\Arr - group()');
        $collection = [
            ['name' => 'Paul', 'age' => 34],
            ['name' => 'Peter', 'age' => 31],
            ['name' => 'John', 'age' => 29],
        ];

        $expected = [
            34 => [
                [
                    'name' => 'Paul',
                    'age'  => 34,
                ],
            ],
            31 => [
                [
                    'name' => 'Peter',
                    'age'  => 31,
                ],
            ],
            29 => [
                [
                    'name' => 'John',
                    'age'  => 29,
                ],
            ],
        ];
        $actual   = Arr::group($collection, 'age');
        $I->assertEquals($expected, $actual);
    }

    /**
     * Tests PhalconNG\Helper\Arr :: group() - object
     *
     * @param UnitTester $I
     */
    public function helperArrGroupObject(UnitTester $I)
    {
        $I->wantToTest('Helper\Arr - group() - object');

        $peter       = new stdClass();
        $peter->name = 'Peter';
        $peter->age  = 34;

        $paul       = new stdClass();
        $paul->name = 'Paul';
        $paul->age  = 31;

        $collection = [
            'peter' => $peter,
            'paul'  => $paul,
        ];


        $expected = [
            'Peter' => [$peter],
            'Paul'  => [$paul],
        ];
        $actual   = Arr::group($collection, 'name');
        $I->assertEquals($expected, $actual);
    }

    /**
     * Tests PhalconNG\Helper\Arr :: group() - function
     *
     * @param UnitTester $I
     */
    public function helperArrGroupFunction(UnitTester $I)
    {
        $I->wantToTest('Helper\Arr - group() - function');
        $collection = ['one', 'two', 'three'];

        $expected = [
            3 => ['one', 'two'],
            5 => ['three'],
        ];
        $actual   = Arr::group($collection, 'strlen');
        $I->assertEquals($expected, $actual);
    }
}

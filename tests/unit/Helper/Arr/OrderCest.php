<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Helper\Arr;

use PhalconNG\Helper\Arr;
use stdClass;
use UnitTester;

/**
 * Class OrderCest
 */
class OrderCest
{
    /**
     * Tests PhalconNG\Helper\Arr :: order()
     *
     * @param UnitTester $I
     */
    public function helperArrOrder(UnitTester $I)
    {
        $I->wantToTest('Helper\Arr - order()');
        $collection = [
            ['id' => 2, 'name' => 'Paul'],
            ['id' => 3, 'name' => 'Peter'],
            ['id' => 1, 'name' => 'John'],
        ];

        $expected = [
            ['id' => 1, 'name' => 'John'],
            ['id' => 2, 'name' => 'Paul'],
            ['id' => 3, 'name' => 'Peter'],
        ];
        $actual   = Arr::order($collection, 'id');
        $I->assertEquals($expected, $actual);

        $expected = [
            ['id' => 3, 'name' => 'Peter'],
            ['id' => 2, 'name' => 'Paul'],
            ['id' => 1, 'name' => 'John'],
        ];
        $actual   = Arr::order($collection, 'id', 'desc');
        $I->assertEquals($expected, $actual);
    }

    /**
     * Tests PhalconNG\Helper\Arr :: order() - object
     *
     * @param UnitTester $I
     */
    public function helperArrOrderObject(UnitTester $I)
    {
        $I->wantToTest('Helper\Arr - order() - object');
        $obj1       = new stdClass();
        $obj1->id   = 2;
        $obj1->name = 'Paul';

        $obj2       = new stdClass();
        $obj2->id   = 3;
        $obj2->name = 'Peter';

        $obj3       = new stdClass();
        $obj3->id   = 1;
        $obj3->name = 'John';

        $collection = [$obj1, $obj2, $obj3];

        $expected = [$obj3, $obj1, $obj2];
        $actual   = Arr::order($collection, 'id');
        $I->assertEquals($expected, $actual);

        $expected = [$obj2, $obj1, $obj3];
        $actual   = Arr::order($collection, 'id', 'desc');
        $I->assertEquals($expected, $actual);
    }
}

<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Helper\Arr;

use PhalconNG\Helper\Arr;
use UnitTester;

/**
 * Class FirstCest
 */
class FirstCest
{
    /**
     * Tests PhalconNG\Helper\Arr :: first()
     *
     * @param UnitTester $I
     */
    public function helperArrFirst(UnitTester $I)
    {
        $I->wantToTest('Helper\Arr - first()');
        $collection = [
            'Phalcon',
            'Framework',
        ];

        $expected = 'Phalcon';
        $actual   = Arr::first($collection);
        $I->assertEquals($expected, $actual);
    }

    /**
     * Tests PhalconNG\Helper\Arr :: first() - function
     *
     * @param UnitTester $I
     */
    public function helperArrFirstFunction(UnitTester $I)
    {
        $I->wantToTest('Helper\Arr - first() - function');
        $collection = [
            'Phalcon',
            'Framework',
        ];

        $expected = 'Framework';
        $actual   = Arr::first(
            $collection,
            function ($element) {
                return strlen($element) > 8;
            }
        );
        $I->assertEquals($expected, $actual);
    }
}

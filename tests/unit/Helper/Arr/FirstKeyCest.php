<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Helper\Arr;

use PhalconNG\Helper\Arr;
use UnitTester;

/**
 * Class FirstKeyCest
 */
class FirstKeyCest
{
    /**
     * Tests PhalconNG\Helper\Arr :: firstKey()
     *
     * @param UnitTester $I
     */
    public function helperArrFirstKey(UnitTester $I)
    {
        $I->wantToTest('Helper\Arr - firstKey()');
        $collection = [
            1 => 'Phalcon',
            3 => 'Framework',
        ];

        $expected = 1;
        $actual   = Arr::firstKey($collection);
        $I->assertEquals($expected, $actual);
    }

    /**
     * Tests PhalconNG\Helper\Arr :: firstKey() - function
     *
     * @param UnitTester $I
     */
    public function helperArrFirstKeyFunction(UnitTester $I)
    {
        $I->wantToTest('Helper\Arr - firstKey() - function');
        $collection = [
            1 => 'Phalcon',
            3 => 'Framework',
        ];

        $expected = 3;
        $actual   = Arr::firstKey(
            $collection,
            function ($element) {
                return strlen($element) > 8;
            }
        );
        $I->assertEquals($expected, $actual);
    }
}

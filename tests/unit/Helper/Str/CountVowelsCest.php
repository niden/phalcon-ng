<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Helper\Str;

use PhalconNG\Helper\Str;
use UnitTester;

/**
 * Class CountVowelsCest
 */
class CountVowelsCest
{
    /**
     * Tests PhalconNG\Helper\Str :: countVowels()
     *
     * @param UnitTester $I
     */
    public function helperStrCountVowels(UnitTester $I)
    {
        $I->wantToTest('Helper\Str - countVowels()');

        $source   = 'Luke, I am your father!';
        $expected = 8;
        $actual   = Str::countVowels($source);
        $I->assertEquals($expected, $actual);

        $source   = '';
        $expected = 0;
        $actual   = Str::countVowels($source);
        $I->assertEquals($expected, $actual);
    }
}

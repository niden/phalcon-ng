<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Helper\Str;

use PhalconNG\Helper\Str;
use UnitTester;

/**
 * Class LowerCest
 */
class LowerCest
{
    /**
     * Tests PhalconNG\Helper\Str :: lower()
     *
     * @param UnitTester $I
     */
    public function helperStrLower(UnitTester $I)
    {
        $I->wantToTest('Helper\Str - lower()');
        $expected = 'hello';
        $actual   = Str::lower('hello');
        $I->assertEquals($expected, $actual);

        $expected = 'hello';
        $actual   = Str::lower('HELLO');
        $I->assertEquals($expected, $actual);

        $expected = '1234';
        $actual   = Str::lower('1234');
        $I->assertEquals($expected, $actual);
    }

    /**
     * Tests PhalconNG\Helper\Str :: lower() - multi-bytes encoding
     *
     * @param UnitTester $I
     */
    public function helperStrLowerMultiBytesEncoding(UnitTester $I)
    {
        $I->wantToTest('Helper\Str - lower() - multi byte encoding');
        $expected = 'привет мир!';
        $actual   = Str::lower('привет мир!');
        $I->assertEquals($expected, $actual);

        $expected = 'привет мир!';
        $actual   = Str::lower('ПриВЕт Мир!');
        $I->assertEquals($expected, $actual);

        $expected = 'привет мир!';
        $actual   = Str::lower('ПРИВЕТ МИР!');
        $I->assertEquals($expected, $actual);


        $expected = 'männer';
        $actual   = Str::lower('männer');
        $I->assertEquals($expected, $actual);

        $expected = 'männer';
        $actual   = Str::lower('mÄnnER');
        $I->assertEquals($expected, $actual);

        $expected = 'männer';
        $actual   = Str::lower('MÄNNER');
        $I->assertEquals($expected, $actual);
    }
}

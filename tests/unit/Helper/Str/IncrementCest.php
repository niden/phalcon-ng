<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Helper\Str;

use PhalconNG\Helper\Str;
use UnitTester;

/**
 * Class IncrementCest
 */
class IncrementCest
{
    /**
     * Tests PhalconNG\Helper\Str :: increment() - string
     *
     * @param UnitTester $I
     */
    public function helperStrIncrementSimpleString(UnitTester $I)
    {
        $I->wantToTest('Helper\Str - increment() - string');
        $source   = 'file';
        $expected = 'file_1';
        $actual   = Str::increment($source);
        $I->assertEquals($expected, $actual);
    }

    /**
     * Tests PhalconNG\Helper\Str :: increment() - already incremented string
     *
     * @param UnitTester $I
     */
    public function helperStrIncrementAlreadyIncremented(UnitTester $I)
    {
        $I->wantToTest('Helper\Str - increment() - already incremented string');
        $source   = 'file_1';
        $expected = 'file_2';
        $actual   = Str::increment($source);
        $I->assertEquals($expected, $actual);
    }

    /**
     * Tests PhalconNG\Helper\Str :: increment() - already incremented string
     * twice
     *
     * @param UnitTester $I
     */
    public function helperStrIncrementAlreadyIncrementedTwice(UnitTester $I)
    {
        $I->wantToTest('Helper\Str - increment() - already incremented string twice');
        $source   = 'file_2';
        $expected = 'file_3';
        $actual   = Str::increment($source);
        $I->assertEquals($expected, $actual);
    }

    /**
     * Tests PhalconNG\Helper\Str :: increment() - string with underscore
     *
     * @param UnitTester $I
     */
    public function helperStrIncrementStringWithUnderscore(UnitTester $I)
    {
        $I->wantToTest('Helper\Str - increment() - string with underscore');
        $source   = 'file_';
        $expected = 'file_1';
        $actual   = Str::increment($source);
        $I->assertEquals($expected, $actual);
    }

    /**
     * Tests PhalconNG\Helper\Str :: increment() - string with a space at the
     * end
     *
     * @param UnitTester $I
     */
    public function helperStrIncrementStringWithSpace(UnitTester $I)
    {
        $I->wantToTest('Helper\Str - increment() - string with a space at the end');
        $source   = 'file ';
        $expected = 'file _1';
        $actual   = Str::increment($source);
        $I->assertEquals($expected, $actual);
    }

    /**
     * Tests PhalconNG\Helper\Str :: increment() - different separator
     *
     * @param UnitTester $I
     */
    public function helperStrIncrementStringWithDifferentSeparator(UnitTester $I)
    {
        $I->wantToTest('Helper\Str - increment() - string with different separator');
        $source   = 'file';
        $expected = 'file-1';
        $actual   = Str::increment($source, '-');
        $I->assertEquals($expected, $actual);
    }
}

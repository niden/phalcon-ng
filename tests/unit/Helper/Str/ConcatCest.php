<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Helper\Str;

use PhalconNG\Helper\Str;
use RuntimeException;
use UnitTester;

/**
 * Class ConcatCest
 */
class ConcatCest
{
    /**
     * Tests PhalconNG\Helper\Str :: concat()
     *
     * @param UnitTester $I
     */
    public function helperStrConcat(UnitTester $I)
    {
        $I->wantToTest('Helper\Str - concat()');
        // Test 1
        $actual   = Str::concat('/', '/tmp/', '/folder_1/', '/folder_2', 'folder_3/');
        $expected = '/tmp/folder_1/folder_2/folder_3/';
        $I->assertEquals($expected, $actual);

        // Test 2
        $actual   = Str::concat('.', '@test.', '.test2.', '.test', '.34');
        $expected = '@test.test2.test.34';
        $I->assertEquals($expected, $actual);
    }

    /**
     * Tests PhalconNG\Helper\Str :: concat() - exception
     *
     * @param UnitTester $I
     */
    public function helperStrConcatException(UnitTester $I)
    {
        $I->wantToTest('Helper\Str - concat() - exception');
        $I->expectThrowable(
            new RuntimeException('concat needs at least three parameters'),
            function () {
                Str::concat('/', '/tmp/');
            }
        );
    }
}

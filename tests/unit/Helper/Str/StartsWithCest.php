<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Helper\Str;

use PhalconNG\Helper\Str;
use UnitTester;

/**
 * Class StartsWithCest
 */
class StartsWithCest
{
    /**
     * Tests PhalconNG\Helper\Str :: startsWith()
     *
     * @param UnitTester $I
     */
    public function helperStrStartsWith(UnitTester $I)
    {
        $I->wantToTest('Helper\Str - startsWith()');
        $actual = Str::startsWith("Hello", "H");
        $I->assertTrue($actual);

        $actual = Str::startsWith("Hello", "He");
        $I->assertTrue($actual);

        $actual = Str::startsWith("Hello", "Hello");
        $I->assertTrue($actual);
    }

    /**
     * Tests PhalconNG\Helper\Str :: startsWith() - empty strings
     *
     * @param UnitTester $I
     */
    public function helperStrStartsWithEmpty(UnitTester $I)
    {
        $I->wantToTest('Helper\Str - startsWith() - empty strings');
        $actual = Str::startsWith("", "");
        $I->assertFalse($actual);
    }

    /**
     * Tests PhalconNG\Helper\Str :: startsWith() - finding an empty string
     *
     * @param UnitTester $I
     */
    public function helperStrStartsWithEmptySearchString(UnitTester $I)
    {
        $I->wantToTest('Helper\Str - startsWith() - search empty string');
        $actual = Str::startsWith("", "hello");
        $I->assertFalse($actual);
    }


    /**
     * Tests PhalconNG\Helper\Str :: startsWith() - case insensitive flag
     *
     * @param UnitTester $I
     */
    public function helperStrStartsWithCaseInsensitive(UnitTester $I)
    {
        $I->wantToTest('Helper\Str - startsWith() - case insensitive flag');
        $actual = Str::startsWith("Hello", "h");
        $I->assertTrue($actual);

        $actual = Str::startsWith("Hello", "he");
        $I->assertTrue($actual);

        $actual = Str::startsWith("Hello", "hello");
        $I->assertTrue($actual);
    }

    /**
     * Tests PhalconNG\Helper\Str :: startsWith() - case sensitive flag
     *
     * @param UnitTester $I
     */
    public function helperStrStartsWithCaseSensitive(UnitTester $I)
    {
        $I->wantToTest('Helper\Str - startsWith() - case sensitive flag');
        $actual = Str::startsWith("Hello", "hello", true);
        $I->assertTrue($actual);

        $actual = Str::startsWith("Hello", "hello", false);
        $I->assertFalse($actual);

        $actual = Str::startsWith("Hello", "h", false);
        $I->assertFalse($actual);
    }
}

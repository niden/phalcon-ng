<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Helper\Str;

use PhalconNG\Helper\Str;
use UnitTester;

/**
 * Class IsLowerCest
 */
class IsLowerCest
{
    /**
     * Tests PhalconNG\Helper\Str :: isLower()
     *
     * @param UnitTester $I
     */
    public function helperStrIsLower(UnitTester $I)
    {
        $I->wantToTest('Helper\Str - isLower()');

        $actual = Str::isLower('phalcon framework');
        $I->assertTrue($actual);

        $actual = Str::isLower('Phalcon Framework');
        $I->assertFalse($actual);
    }
}

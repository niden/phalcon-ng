<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Helper\Str;

use PhalconNG\Helper\Str;
use UnitTester;

/**
 * Class IsAnagramCest
 */
class IsAnagramCest
{
    /**
     * Tests PhalconNG\Helper\Str :: isAnagram()
     *
     * @param UnitTester $I
     */
    public function helperStrIsAnagram(UnitTester $I)
    {
        $I->wantToTest('Helper\Str - isAnagram()');

        $actual = Str::isAnagram('rail safety', 'fairy tales');
        $I->assertTrue($actual);
    }
}

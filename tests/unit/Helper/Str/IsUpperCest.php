<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Helper\Str;

use PhalconNG\Helper\Str;
use UnitTester;

/**
 * Class IsUpperCest
 */
class IsUpperCest
{
    /**
     * Tests PhalconNG\Helper\Str :: isUpper()
     *
     * @param UnitTester $I
     */
    public function helperStrIsUpper(UnitTester $I)
    {
        $I->wantToTest('Helper\Str - isUpper()');

        $actual = Str::isUpper('PHALCON FRAMEWORK');
        $I->assertTrue($actual);

        $actual = Str::isUpper('Phalcon Framework');
        $I->assertFalse($actual);
    }
}

<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Helper\Str;

use PhalconNG\Helper\Str;
use UnitTester;

/**
 * Class FolderFromFileCest
 */
class FolderFromFileCest
{
    /**
     * Tests PhalconNG\Helper\Str :: folderFromFile()
     *
     * @param UnitTester $I
     */
    public function helperStrFolderFromFile(UnitTester $I)
    {
        $I->wantToTest('Helper\Str - folderFromFile()');
        $fileName = 'abcdef12345.jpg';

        $expected = 'ab/cd/ef/12/3/';
        $actual   = Str::folderFromFile($fileName);
        $I->assertEquals($expected, $actual);
    }

    /**
     * Tests PhalconNG\Helper\Str :: folderFromFile() - empty string
     *
     * @param UnitTester $I
     */
    public function helperStrFolderFromFileEmptyString(UnitTester $I)
    {
        $I->wantToTest('Helper\Str - folderFromFile() - empty string');
        $fileName = '';

        $expected = '/';
        $actual   = Str::folderFromFile($fileName);
        $I->assertEquals($expected, $actual);
    }
}

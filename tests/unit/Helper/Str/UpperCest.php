<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Helper\Str;

use PhalconNG\Helper\Str;
use UnitTester;

/**
 * Class UpperCest
 */
class UpperCest
{
    /**
     * Tests PhalconNG\Helper\Str :: upper()
     *
     * @param UnitTester $I
     */
    public function helperStrUpper(UnitTester $I)
    {
        $I->wantToTest('Helper\Str - upper()');
        $expected = 'HELLO';
        $actual   = Str::upper('hello');
        $I->assertEquals($expected, $actual);

        $expected = 'HELLO';
        $actual   = Str::upper('HELLO');
        $I->assertEquals($expected, $actual);

        $expected = '1234';
        $actual   = Str::upper('1234');
        $I->assertEquals($expected, $actual);
    }

    /**
     * Tests PhalconNG\Helper\Str :: upper() - multi-bytes encoding
     *
     * @param UnitTester $I
     */
    public function helperStrUpperMultiBytesEncoding(UnitTester $I)
    {
        $I->wantToTest('Helper\Str - upper() - multi byte encoding');
        $expected = 'ПРИВЕТ МИР!';
        $actual   = Str::upper('ПРИВЕТ МИР!');
        $I->assertEquals($expected, $actual);

        $expected = 'ПРИВЕТ МИР!';
        $actual   = Str::upper('ПриВЕт Мир!');
        $I->assertEquals($expected, $actual);

        $expected = 'ПРИВЕТ МИР!';
        $actual   = Str::upper('привет мир!');
        $I->assertEquals($expected, $actual);

        $expected = 'MÄNNER';
        $actual   = Str::upper('MÄNNER');
        $I->assertEquals($expected, $actual);

        $expected = 'MÄNNER';
        $actual   = Str::upper('mÄnnER');
        $I->assertEquals($expected, $actual);

        $expected = 'MÄNNER';
        $actual   = Str::upper('männer');
        $I->assertEquals($expected, $actual);
    }
}

<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Unit\Helper\Str;

use PhalconNG\Helper\Str;
use UnitTester;

/**
 * Class ReduceSlashesCest
 */
class ReduceSlashesCest
{
    /**
     * Tests PhalconNG\Helper\Str :: reduceSlashes()
     *
     * @param UnitTester $I
     */
    public function helperStrReduceSlashes(UnitTester $I)
    {
        $I->wantToTest('Helper\Str - reduceSlashes()');
        $expected = 'app/controllers/IndexController';
        $actual   = Str::reduceSlashes('app/controllers//IndexController');
        $I->assertEquals($expected, $actual);

        $expected = 'http://foo/bar/baz/buz';
        $actual   = Str::reduceSlashes('http://foo//bar/baz/buz');
        $I->assertEquals($expected, $actual);

        $expected = 'php://memory';
        $actual   = Str::reduceSlashes('php://memory');
        $I->assertEquals($expected, $actual);

        $expected = 'http/https';
        $actual   = Str::reduceSlashes('http//https');
        $I->assertEquals($expected, $actual);
    }
}

<?php
declare(strict_types=1);

/**
 * This file is part of the Phalcon Framework.
 *
 * (c) Phalcon Team <team@phalconphp.com>
 *
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace PhalconNG\Test\Fixtures\Traits;

use PhalconNG\Logger\Adapter\Stream;
use PhalconNG\Logger\Exception;
use PhalconNG\Logger\Logger;
use UnitTester;
use function outputDir;

/**
 * Trait LoggerTrait
 *
 * @package PhalconNG\Test\Fixtures\Traits
 */
trait LoggerTrait
{
    /**
     * @param UnitTester $I
     * @param string     $level
     *
     * @throws Exception
     */
    protected function runLoggerFile(UnitTester $I, string $level)
    {
        $logPath  = outputDir();
        $fileName = $I->getNewFileName('log', 'log');
        $adapter  = new Stream($logPath . $fileName);

        $logString = "Hello";

        $logger  = new Logger('my-logger', ['one' => $adapter]);
        $logTime = date('D, d M y H:i:s O');
        $logger->{$level}($logString);

        $logger->getAdapter('one')->close();

        $I->amInPath($logPath);
        $I->openFile($fileName);
        $expected = sprintf(
            "[%s][%s] " . $logString,
            $logTime,
            $level
        );

        $I->seeInThisFile($expected);
        $I->safeDeleteFile($fileName);
    }
}
